package fileStore.web.security;

public enum FileAccessLevel {
	
	NO_ACCESS(0), VIEW_ACCESS(1), DOWNLOAD_ACCESS(2), CHANGE_ACCESS(3), OWNER(4);
	
	private int accessLevel;

	private FileAccessLevel(int accessLevel) {
		this.accessLevel = accessLevel;
	}

	public int getAccessLevel() {
		return accessLevel;
	}
	
	public static FileAccessLevel parse(Integer accessLevel) {
		for (FileAccessLevel fal : FileAccessLevel.values()) {
			if (fal.accessLevel == accessLevel)
				return fal;
		}
		return NO_ACCESS;
	}
}
