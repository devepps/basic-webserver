package fileStore.web.security.contexts;

import fileStore.repository.types.SectionNode;
import permission.SingleContentContext;

public class SectionContext extends SingleContentContext<SectionNode> {

	public SectionContext(SectionNode content) {
		super(content);
	}

	@Override
	public boolean isWorkable() {
		return super.isWorkable() && this.getContent().getId() != null && this.getContent().getUserId() != null;
	}

}
