package fileStore.web.security.contexts;

import permission.SingleContentContext;
import userSystem.data.ValidatedUser;

public class UserContext extends SingleContentContext<ValidatedUser> {

	public UserContext(ValidatedUser content) {
		super(content);
	}

	@Override
	public boolean isWorkable() {
		return super.isWorkable() && this.getContent().getValidationKey() != null && this.getContent().getUser() != null
				&& this.getContent().getUser().getId() != null;
	}

}
