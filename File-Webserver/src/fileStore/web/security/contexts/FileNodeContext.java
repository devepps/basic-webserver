package fileStore.web.security.contexts;

import fileStore.repository.types.FileNode;
import permission.SingleContentContext;

public class FileNodeContext extends SingleContentContext<FileNode> {

	public FileNodeContext(FileNode content) {
		super(content);
	}

	@Override
	public boolean isWorkable() {
		return super.isWorkable() && this.getContent().getId() != null && this.getContent().getUserId() != null;
	}

}
