package fileStore.web.security.contexts;

import fileStore.repository.types.FolderNode;
import permission.SingleContentContext;

public class FolderContext extends SingleContentContext<FolderNode> {

	public FolderContext(FolderNode content) {
		super(content);
	}

	@Override
	public boolean isWorkable() {
		return super.isWorkable() && this.getContent().getId() != null && this.getContent().getUserId() != null;
	}

}
