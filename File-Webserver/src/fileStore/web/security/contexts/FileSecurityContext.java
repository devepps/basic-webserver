package fileStore.web.security.contexts;

import fileStore.repository.types.FileNode;
import permission.Context;
import userSystem.data.ValidatedUser;

public class FileSecurityContext extends Context {

	private FileNodeContext fileNodeContext;
	private UserContext userContext;

	public FileSecurityContext(FileNodeContext fileNodeContext, UserContext userContext) {
		super();
		this.fileNodeContext = fileNodeContext;
		this.userContext = userContext;
	}

	public FileSecurityContext(FileNode fileNode, ValidatedUser currentUser) {
		this.fileNodeContext = new FileNodeContext(fileNode);
		this.userContext = new UserContext(currentUser);
	}

	public FileNodeContext getFileNodeContext() {
		return fileNodeContext;
	}

	public UserContext getUserContext() {
		return userContext;
	}

	public boolean isWorkable() {
		return (fileNodeContext != null && fileNodeContext.isWorkable())
				&& (userContext != null && userContext.isWorkable());
	}

}
