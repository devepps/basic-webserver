package fileStore.web.security.contexts;

import fileStore.repository.types.FolderNode;
import fileStore.repository.types.SectionNode;
import permission.Context;
import userSystem.data.ValidatedUser;

public class FolderSecurityContext extends Context {

	private FolderContext folderContext;
	private SectionContext sectionContext;
	private UserContext userContext;

	public FolderSecurityContext(FolderContext folderContext, SectionContext sectionContext, UserContext userContext) {
		super();
		this.folderContext = folderContext;
		this.sectionContext = sectionContext;
		this.userContext = userContext;
	}

	public FolderSecurityContext(FolderNode folder, SectionNode section, ValidatedUser user) {
		super();
		this.folderContext = new FolderContext(folder);
		this.sectionContext = new SectionContext(section);
		this.userContext = new UserContext(user);
	}

	public FolderContext getFolderContext() {
		return folderContext;
	}

	public SectionContext getSectionContext() {
		return sectionContext;
	}

	public UserContext getUserContext() {
		return userContext;
	}

	public boolean isWorkable() {
		return (userContext != null && userContext.isWorkable())
				&& ((folderContext != null && folderContext.isWorkable())
						|| (sectionContext != null && sectionContext.isWorkable()));
	}

}
