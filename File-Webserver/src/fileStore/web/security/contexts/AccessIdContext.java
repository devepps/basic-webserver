package fileStore.web.security.contexts;

import permission.SingleContentContext;

public class AccessIdContext extends SingleContentContext<String> {

	public AccessIdContext(String accessId) {
		super(accessId);
	}
}
