package fileStore.web.security.permissions;

import fileStore.repository.FileType;

public class FileInfo {

	private Integer fileId;
	private FileType fileType;
	private boolean isOwner;

	public FileInfo(Integer fileId, FileType fileType, boolean isOwner) {
		super();
		this.fileId = fileId;
		this.fileType = fileType;
		this.isOwner = isOwner;
	}

	public Integer getFileId() {
		return fileId;
	}

	public FileType getFileType() {
		return fileType;
	}

	public boolean isOwner() {
		return isOwner;
	}

	@Override
	public String toString() {
		return "FileInfo [fileId=" + fileId + ", fileType=" + fileType + ", isOwner=" + isOwner + "]";
	}

}
