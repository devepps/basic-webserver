package fileStore.web.security.permissions;

import static fileStore.web.security.FileAccessLevel.DOWNLOAD_ACCESS;

import fileStore.web.security.contexts.AccessIdContext;
import fileStore.web.security.contexts.UserContext;
import permission.ContextHandler;

public class FileDownloadPermission extends FilePermission {

	@Override
	public boolean hasPermission(ContextHandler ch) {
		FileInfo fileInfo = this.getFileInfo(ch);
		if (fileInfo.isOwner()) {
			return true;
		}

		// <== Test Valid AccessId ==>
		AccessIdContext aic = ch.getContext(AccessIdContext.class);
		if (aic != null && aic.isWorkable()) {
			String accessId = aic.getContent();

			if (DOWNLOAD_ACCESS.getAccessLevel() <= this
					.findIdAccess(accessId, fileInfo.getFileId(), fileInfo.getFileType()).getAccessLevel())
				return true;
		}

		// <== Test User Access ==>
		UserContext userContext = ch.getContext(UserContext.class);
		if (userContext != null && userContext.isWorkable()) {
			if (DOWNLOAD_ACCESS.getAccessLevel() <= this
					.findUserAccess(userContext.getContent(), fileInfo.getFileId(), fileInfo.getFileType())
					.getAccessLevel())
				return true;
		}

		return false;
	}
}
