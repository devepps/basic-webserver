package fileStore.web.security.permissions;

import java.util.ArrayList;

import fileStore.repository.FSAIdRepository;
import fileStore.repository.FSAUserRepository;
import fileStore.repository.FileLocationInfoRepository;
import fileStore.repository.FileType;
import fileStore.repository.types.FileLocationInfo;
import fileStore.repository.types.FileNode;
import fileStore.repository.types.FileSecureAccessId;
import fileStore.repository.types.FileSecureAccessUser;
import fileStore.repository.types.FolderNode;
import fileStore.repository.types.SectionNode;
import fileStore.web.security.FileAccessLevel;
import fileStore.web.security.contexts.FileSecurityContext;
import fileStore.web.security.contexts.FolderSecurityContext;
import permission.ContextHandler;
import permission.Permission;
import userSystem.data.ValidatedUser;

public abstract class FilePermission extends Permission {

	private final FSAIdRepository fsaIdRepo = FSAIdRepository.FSA_ID_REPOSITORY;
	private final FSAUserRepository fsaUserRepo = FSAUserRepository.FSA_USER_REPOSITORY;
	private final FileLocationInfoRepository fliRepo = FileLocationInfoRepository.FILE_LOCATION_INFO_REPOSITORY;

	public FileAccessLevel findIdAccess(String accessId, Integer fileId, FileType fileType) {
		ArrayList<FileSecureAccessId> fsaIds = this.fsaIdRepo.getFSAIds(accessId);
		FileAccessLevel accessLevel = FileAccessLevel.NO_ACCESS;
		if (fsaIds != null) {
			for (FileSecureAccessId fsaId : fsaIds) {
				if (!fsaId.isExpired()) {
					if (!fsaId.isExpired() && checkFile(fsaId.getTargetId(), fsaId.getFileType(), fileId, fileType)) {
						accessLevel = fsaId.getFileAccessLevel().getAccessLevel() > accessLevel.getAccessLevel()
								? fsaId.getFileAccessLevel() : accessLevel;
					}
				}
			}
		}
		return accessLevel;
	}

	public FileAccessLevel findUserAccess(ValidatedUser user, Integer fileId, FileType fileType) {
		ArrayList<FileSecureAccessUser> fsaUsers = fsaUserRepo.getFSAUsers(user);
		FileAccessLevel accessLevel = FileAccessLevel.NO_ACCESS;
		if (fsaUsers != null) {
			for (FileSecureAccessUser fsaUser : fsaUsers) {
				if (!fsaUser.isExpired() && checkFile(fsaUser.getTargetId(), fsaUser.getFileType(), fileId, fileType)) {
					accessLevel = fsaUser.getFileAccessLevel().getAccessLevel() > accessLevel.getAccessLevel()
							? fsaUser.getFileAccessLevel() : accessLevel;
				}
			}
		}
		return accessLevel;
	}

	private boolean checkFile(Integer targetId, FileType targetFileType, Integer fileId, FileType fileType) {
		if (fileType == targetFileType && targetId == fileId) {
			return true;
		}
		
		ArrayList<FileLocationInfo> subFiles = this.fliRepo.getFileLocationInfoFromHead(targetId);

		for (FileLocationInfo info: subFiles) {
			if (info.getTargetFileType() == fileType && info.getTargetFileId() == fileId) {
				return true;
			} else if (info.getTargetFileType() == FileType.FOLDER) {
				if (checkFile(info.getTargetFileId(), FileType.FOLDER, fileId, fileType))
					return true;
			}
		}
		return false;
	}

	public boolean isOwnerOfFile(FileNode fileNode, ValidatedUser user) {
		return user.getUser().getId().equals(fileNode.getUserId());
	}

	public boolean isOwnerOfSection(SectionNode sectionNode, ValidatedUser user) {
		return user.getUser().getId().equals(sectionNode.getUserId());
	}

	public boolean isOwnerOfFolder(FolderNode folderNode, ValidatedUser user) {
		return user.getUser().getId().equals(folderNode.getUserId());
	}
	
	public FileInfo getFileInfo(ContextHandler ch) {
		FileSecurityContext fileSC = ch.getContext(FileSecurityContext.class);
		FolderSecurityContext folderSC = ch.getContext(FolderSecurityContext.class);
		
		Integer fileId = null;
		FileType fileType = null;
		boolean isOwner = false;
		if (fileSC != null && fileSC.isWorkable()) {
			FileNode node = fileSC.getFileNodeContext().getContent();
			if (this.isOwnerOfFile(node, fileSC.getUserContext().getContent())) {
				isOwner = true;
			}
			fileId = node.getId();
			fileType = FileType.FILE;
		} else if (folderSC != null && folderSC.isWorkable()) {
			if (folderSC.getFolderContext() != null && folderSC.getFolderContext().isWorkable()) {
				FolderNode node = folderSC.getFolderContext().getContent();
				if (this.isOwnerOfFolder(node, fileSC.getUserContext().getContent())) {
					isOwner = true;
				}
				fileId = node.getId();
				fileType = FileType.FOLDER;
			} else if (folderSC.getSectionContext() != null && folderSC.getSectionContext().isWorkable()) {
				SectionNode node = folderSC.getSectionContext().getContent();
				if (this.isOwnerOfSection(node, fileSC.getUserContext().getContent())) {
					isOwner = true;
				}
				fileId = node.getId();
				fileType = FileType.SECTION;
			}
		}
		
		return new FileInfo(fileId, fileType, isOwner);
	}
}
