package fileStore.web.security;

import fileStore.web.security.permissions.FileDownloadPermission;
import fileStore.web.security.permissions.FileEditPermission;
import fileStore.web.security.permissions.FileViewPermission;
import permission.Permission;

public enum PermissionHolder {
	
	FILE_VIEW_PH(new FileViewPermission()), FILE_DOWNLOAD_PH(new FileDownloadPermission()), FILE_EDIT_PH(new FileEditPermission());
	
	private Permission permission;

	private PermissionHolder(Permission permission) {
		this.permission = permission;
	}

	public Permission getPermission() {
		return permission;
	}

}
