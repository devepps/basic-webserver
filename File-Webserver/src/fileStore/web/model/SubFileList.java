package fileStore.web.model;

import java.util.Arrays;

public class SubFileList {

	private Integer[] subFiles;
	private Integer[] subFolders;

	public SubFileList(Integer[] subFiles, Integer[] subFolders) {
		super();
		this.subFiles = subFiles;
		this.subFolders = subFolders;
	}

	public Integer[] getSubFiles() {
		return subFiles;
	}

	public Integer[] getSubFolders() {
		return subFolders;
	}

	@Override
	public String toString() {
		return "SubFileList [subFiles=" + Arrays.toString(subFiles) + ", subFolders=" + Arrays.toString(subFolders)
				+ "]";
	}

}
