package fileStore.web.model;

import java.util.Random;

import collection.LetterSymbolsCharset;

public class SecurityTempId {

	private static final long VALID_DURATION = 1000 * 60;

	private String id;
	private long created;

	public SecurityTempId() {
		super();
		byte[] bytes = new byte[500];
		new Random().nextBytes(bytes);
		this.id = new String(bytes, LetterSymbolsCharset.OWN);
		this.created = System.currentTimeMillis();
	}

	public String getId() {
		return id;
	}

	public long getCreated() {
		return created;
	}

	public boolean isValid() {
		return System.currentTimeMillis() - created < VALID_DURATION;
	}

	@Override
	public String toString() {
		return "TempUrl [id=" + id + ", created=" + created + "]";
	}

}
