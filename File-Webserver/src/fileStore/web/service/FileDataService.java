package fileStore.web.service;

import static fileStore.web.security.PermissionHolder.*;

import java.io.InputStream;

import authorisation.web.exceptions.NotLoggedinException;
import authorisation.web.service.SecurityService;
import fileStore.java.FileStore;
import fileStore.logger.FileServerLogger;
import fileStore.repository.FileNodeRepository;
import fileStore.repository.FileType;
import fileStore.repository.types.FileNode;
import fileStore.web.dto.FileNodeDTO;
import fileStore.web.exceptions.InvalidFileException;
import fileStore.web.exceptions.UnknownFileNodeException;
import fileStore.web.security.contexts.FileSecurityContext;
import permission.ContextHandler;
import server.data.streams.TransferStream;
import userSystem.data.ValidatedUser;

public class FileDataService {

	private final FileNodeRepository fileNodeRepository = FileNodeRepository.FILE_NODE_REPOSITORY;

	private final FileStore fileStore = new FileStore();
	
	private SecurityService securityService;
	private FileNodeService fileNodeService;
	private FolderService folderService;
	
	public FileDataService create(SecurityService securityService, FileNodeService fileNodeService, FolderService folderService) {
		this.securityService = securityService;
		this.fileNodeService = fileNodeService;
		this.folderService = folderService;
		return this;
	}

	public TransferStream getFileData(FileNode fileNode) {
		return this.fileStore.getFile(fileNode);
	}

	public FileNodeDTO addFileData(InputStream data, FileNodeDTO fileNodeDTO, Integer folderNodeId, boolean toFolder) {
		FileServerLogger.debug("Attemps to add new fileData");
		if (data == null) {
			throw new InvalidFileException();
		}
		FileNode fileNode = toFolder ? fileNodeService.addFileNodeToFolder(fileNodeDTO, folderNodeId) :
			fileNodeService.addFileNodeToSection(fileNodeDTO, folderNodeId);

		long size = fileStore.addFile(data, fileNode);
		FileServerLogger.debug("FileData saved: " + size);
		if (size == -1) {
			fileNodeRepository.removeFileNode(fileNode.getId());
			throw new InvalidFileException();
		}

		fileNode.setSize((int) size);
		return fileNodeService.updateFileNode(fileNode.getId(), new FileNodeDTO(fileNode));
	}

	public void deleteFileNode(Integer id) {
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}
		if (id == null) {
			throw new UnknownFileNodeException();
		}

		FileNode fileNode = fileNodeRepository.getFileNode(id);
		if (fileNode == null) {
			throw new UnknownFileNodeException();
		}
		
		// Insert Security Check
		
		this.folderService.deleteFolderCompleteBypassSecurity(fileNode.getId(), FileType.FILE);
	}

	public void deleteFileNodeBypassSecurity(FileNode fileNode) {
		fileNodeRepository.removeFileNode(fileNode.getId());
		fileStore.removeFile(fileNode);
	}

	public FileNodeDTO updateFileData(InputStream data, Integer id) {
		if (data == null) {
			throw new InvalidFileException();
		}
		FileNode fileNode = fileNodeService.getFileNode(id);
		FILE_EDIT_PH.getPermission()
				.checkPermission(new ContextHandler(new FileSecurityContext(fileNode, securityService.currentUser())));

		long size = fileStore.updateFile(data, fileNode);
		if (size == -1) {
			throw new InvalidFileException();
		}

		fileNode.setSize((int) size);
		return fileNodeService.updateFileNode(fileNode.getId(), new FileNodeDTO(fileNode));
	}
}
