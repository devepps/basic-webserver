package fileStore.web.service;

import static fileStore.web.security.PermissionHolder.*;

import java.util.ArrayList;

import authorisation.web.exceptions.NotLoggedinException;
import authorisation.web.service.SecurityService;
import fileStore.logger.FileServerLogger;
import fileStore.repository.FileLocationInfoRepository;
import fileStore.repository.FileNodeRepository;
import fileStore.repository.FileType;
import fileStore.repository.FolderNodeRepository;
import fileStore.repository.SectionNodeRepository;
import fileStore.repository.types.FileLocationInfo;
import fileStore.repository.types.FileNode;
import fileStore.repository.types.FolderNode;
import fileStore.repository.types.SectionNode;
import fileStore.web.dto.FileNodeDTO;
import fileStore.web.exceptions.FolderNotFoundException;
import fileStore.web.exceptions.InvalidFileNodeException;
import fileStore.web.exceptions.UnknownFileNodeException;
import fileStore.web.security.contexts.FileSecurityContext;
import permission.ContextHandler;
import userSystem.data.ValidatedUser;

public class FileNodeService {

	private final FileNodeRepository fileNodeRepository = FileNodeRepository.FILE_NODE_REPOSITORY;
	private final FolderNodeRepository folderNodeRepository = FolderNodeRepository.FOLDER_NODE_REPOSITORY;
	private final SectionNodeRepository sectionNodeRepository = SectionNodeRepository.SECTION_NODE_REPOSITORY;
	private final FileLocationInfoRepository fliRepo = FileLocationInfoRepository.FILE_LOCATION_INFO_REPOSITORY;

	private SecurityService securityService;

	public FileNodeService create(SecurityService securityService) {
		this.securityService = securityService;
		return this;
	}

	public ArrayList<FileNodeDTO> getFileNodes() {
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}

		ArrayList<FileNode> fileNodes = fileNodeRepository.getFileNodes(currentUser);
		ArrayList<FileNodeDTO> fileNodeDTOs = new ArrayList<>();

		for (FileNode fileNode : fileNodes) {
			try {
				FILE_VIEW_PH.getPermission()
						.checkPermission(new ContextHandler(new FileSecurityContext(fileNode, currentUser)));
				fileNodeDTOs.add(new FileNodeDTO(fileNode));
			} catch (permission.NoPermissionException e) {
			}
		}
		return fileNodeDTOs;
	}

	public FileNodeDTO updateFileNode(Integer id, FileNodeDTO fileNodeDTO) {
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}
		if (id == null) {
			throw new UnknownFileNodeException();
		}
		if (fileNodeDTO.getDataType() == null || fileNodeDTO.getName() == null) {
			throw new InvalidFileNodeException();
		}

		FileNode fileNode = fileNodeRepository.getFileNode(id);
		if (fileNode == null) {
			throw new UnknownFileNodeException();
		}
		FILE_EDIT_PH.getPermission()
				.checkPermission(new ContextHandler(new FileSecurityContext(fileNode, currentUser)));

		fileNode.update(fileNodeDTO);
		fileNodeRepository.updateFileNode(fileNode);

		return new FileNodeDTO(fileNode);
	}

	public FileNode getFileNode(Integer id) {
		FileServerLogger.debug("Attemps to read fileNode: " + id);
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}
		if (id == null) {
			throw new UnknownFileNodeException();
		}
		FileServerLogger.debug("FileNode-Request accepted");

		FileNode fileNode = fileNodeRepository.getFileNode(id);
		FileServerLogger.debug("FileNode (" + id + ") has been found: " + fileNode);
		if (fileNode == null) {
			throw new UnknownFileNodeException();
		}
		FILE_VIEW_PH.getPermission()
				.checkPermission(new ContextHandler(new FileSecurityContext(fileNode, currentUser)));

		return fileNode;
	}

	public FileNode addFileNodeToFolder(FileNodeDTO fileNodeDTO, Integer folderNodeId) {
		FileServerLogger.debug("Attemps to add a new fileNode: \"" + fileNodeDTO + "\" to folder: " + folderNodeId);
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}
		if (fileNodeDTO.getDataType() == null || fileNodeDTO.getName() == null || folderNodeId == null) {
			throw new InvalidFileNodeException();
		}

		FolderNode folderNode = folderNodeRepository.getFolderNode(folderNodeId);
		FileServerLogger.debug("FolderNode (" + folderNodeId + ") has been found: " + folderNode);
		if (folderNode == null) {
			throw new FolderNotFoundException();
		}
		// Insert Security Check

		FileNode fileNode = new FileNode(fileNodeDTO, currentUser);
		fileNode.setSize(0);
		fileNode = fileNodeRepository.addFileNode(fileNode);
		FileServerLogger.debug("FileNode \"" + fileNode + "\" has been saved");

		FileLocationInfo folderInfo = fliRepo.getFileLocationInfoForFile(folderNode.getId(), FileType.FOLDER);
		fliRepo.addFileLocationInfo(new FileLocationInfo(folderInfo.getPath() + '/' + fileNode.getName() + "." + fileNode.getDataType(), null, fileNode, folderNode));
		FileServerLogger.debug("FolderNode \"" + folderNode + "\" has been updated with new fileNode");

		return fileNode;
	}

	public FileNode addFileNodeToSection(FileNodeDTO fileNodeDTO, Integer sectionNodeId) {
		FileServerLogger.debug("Attemps to add a new fileNode: \"" + fileNodeDTO + "\" to section: " + sectionNodeId);
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}
		if (fileNodeDTO.getDataType() == null || fileNodeDTO.getName() == null || sectionNodeId == null) {
			throw new InvalidFileNodeException();
		}

		SectionNode sectionNode = sectionNodeRepository.getSectionNode(sectionNodeId);
		FileServerLogger.debug("SectionNode (" + sectionNodeId + ") has been found: " + sectionNode);
		if (sectionNode == null) {
			throw new FolderNotFoundException();
		}
		// Insert Security Check

		FileNode fileNode = new FileNode(fileNodeDTO, currentUser);
		fileNode.setSize(0);
		fileNode = fileNodeRepository.addFileNode(fileNode);
		FileServerLogger.debug("FileNode \"" + fileNode + "\" has been saved");

		fliRepo.addFileLocationInfo(new FileLocationInfo(sectionNode.getName() + "/" + fileNode.getName() + "." + fileNode.getDataType(), null, fileNode, sectionNode));
		FileServerLogger.debug("SectionNode \"" + sectionNode + "\" has been updated with new fileNode");

		return fileNode;
	}

	public FileNode getFileNodeBypassSecurity(Integer id) {
		if (id == null) {
			throw new UnknownFileNodeException();
		}
		FileNode fileNode = fileNodeRepository.getFileNode(id);
		if (fileNode == null) {
			throw new UnknownFileNodeException();
		}
		return fileNode;
	}
}
