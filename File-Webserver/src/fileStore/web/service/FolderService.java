package fileStore.web.service;

import java.time.LocalDateTime;
import java.util.ArrayList;

import authorisation.web.exceptions.NotLoggedinException;
import authorisation.web.service.SecurityService;
import fileStore.logger.FileServerLogger;
import fileStore.repository.FileLocationInfoRepository;
import fileStore.repository.FileNodeRepository;
import fileStore.repository.FileType;
import fileStore.repository.FolderNodeRepository;
import fileStore.repository.SectionNodeRepository;
import fileStore.repository.types.FileLocationInfo;
import fileStore.repository.types.FolderNode;
import fileStore.repository.types.SectionNode;
import fileStore.web.dto.FolderNodeDTO;
import fileStore.web.dto.SectionNodeDTO;
import fileStore.web.exceptions.FolderNotFoundException;
import fileStore.web.exceptions.InvalidDataException;
import fileStore.web.exceptions.SectionNotFoundException;
import fileStore.web.model.SubFileList;
import userSystem.data.ValidatedUser;

public class FolderService {

	private final SectionNodeRepository sectionNodeRepository = SectionNodeRepository.SECTION_NODE_REPOSITORY;
	private final FolderNodeRepository folderNodeRepository = FolderNodeRepository.FOLDER_NODE_REPOSITORY;
	private final FileNodeRepository fileNodeRepository = FileNodeRepository.FILE_NODE_REPOSITORY;
	private final FileLocationInfoRepository fliRepo = FileLocationInfoRepository.FILE_LOCATION_INFO_REPOSITORY;

	private SecurityService securityService;
	private FileDataService fileDataService;
	
	public FolderService create(SecurityService securityService, FileDataService fileDataService) {
		this.securityService = securityService;
		this.fileDataService = fileDataService;
		return this;
	}

	public SectionNodeDTO[] getSections() {
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}

		ArrayList<SectionNode> nodes = sectionNodeRepository.getSectionNodes(currentUser);
		if (nodes == null) {
			throw new SectionNotFoundException();
		}
		SectionNodeDTO[] data = new SectionNodeDTO[nodes.size()];
		for (int i = 0; i < data.length; i++) {
			// Insert Security Check
			SectionNode node = nodes.get(i);
			data[i] = new SectionNodeDTO(node, fliRepo.getSubFiles(node.getId(), FileType.SECTION));
		}

		return data;
	}

	public FolderNodeDTO getFolder(Integer folderId) {
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}

		if (folderId == null) {
			throw new InvalidDataException();
		}
		FolderNode node = folderNodeRepository.getFolderNode(folderId);
		if (node == null) {
			throw new FolderNotFoundException();
		}
		// Insert Security Check

		return new FolderNodeDTO(node, fliRepo.getSubFiles(node.getId(), FileType.FOLDER));
	}

	public SectionNodeDTO addSection(SectionNodeDTO nodeDTO) {
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}

		if (nodeDTO == null || nodeDTO.getName() == null) {
			throw new InvalidDataException();
		}

		SectionNode node = new SectionNode(0, nodeDTO.getName(), currentUser.getUser().getId(), LocalDateTime.now());
		node = sectionNodeRepository.addSectionNode(node);

		return new SectionNodeDTO(node, new SubFileList(new Integer[0], new Integer[0]));
	}

	public FolderNodeDTO addFolder(FolderNodeDTO folderNodeDTO, SectionNodeDTO sectionNodeDTO) {
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}

		if (folderNodeDTO == null || folderNodeDTO.getName() == null) {
			throw new InvalidDataException();
		}
		if (sectionNodeDTO == null || sectionNodeDTO.getId() == null) {
			throw new InvalidDataException();
		}

		SectionNode sectionNode = sectionNodeRepository.getSectionNode(sectionNodeDTO.getId());
		if (sectionNode == null) {
			throw new SectionNotFoundException();
		}
		// Insert Security Check

		FolderNode folderNode = new FolderNode(0, folderNodeDTO.getName(), currentUser.getUser().getId(),
				LocalDateTime.now());
		folderNode = folderNodeRepository.addFolderNode(folderNode);

		fliRepo.addFileLocationInfo(new FileLocationInfo(sectionNode.getName() + "/" + folderNode.getName(), null, folderNode, sectionNode));

		return new FolderNodeDTO(folderNode, new SubFileList(new Integer[0], new Integer[0]));
	}

	public FolderNodeDTO addFolder(FolderNodeDTO folderNodeDTO, FolderNodeDTO headFolderNodeDTO) {
		FileServerLogger.debug("Attempts to add a Folder: " + folderNodeDTO);
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}

		if (folderNodeDTO == null || folderNodeDTO.getName() == null) {
			throw new InvalidDataException();
		}
		if (headFolderNodeDTO == null || headFolderNodeDTO.getId() == null) {
			throw new InvalidDataException();
		}

		FolderNode headFolderNode = folderNodeRepository.getFolderNode(headFolderNodeDTO.getId());
		FileServerLogger.debug("Head-Folder: " + headFolderNode);
		if (headFolderNode == null) {
			throw new SectionNotFoundException();
		}
		// Insert Security Check
		FileLocationInfo headFolderNodeLocationInfo = fliRepo.getFileLocationInfoForFile(headFolderNode.getId(), FileType.FOLDER);
		FileServerLogger.debug("Head Location: " + headFolderNodeLocationInfo);

		FolderNode folderNode = new FolderNode(0, folderNodeDTO.getName(), currentUser.getUser().getId(),
				LocalDateTime.now());
		folderNode = folderNodeRepository.addFolderNode(folderNode);
		
		fliRepo.addFileLocationInfo(new FileLocationInfo(headFolderNodeLocationInfo.getPath() + '/' + headFolderNode.getName(), null, folderNode, headFolderNode));

		return new FolderNodeDTO(folderNode, new SubFileList(new Integer[0], new Integer[0]));
	}

	public SectionNodeDTO updateSection(SectionNodeDTO sectionNodeDTO, Integer id) {
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}

		if (sectionNodeDTO == null || sectionNodeDTO.getName() == null || id == null) {
			throw new InvalidDataException();
		}

		SectionNode sectionNode = sectionNodeRepository.getSectionNode(id);
		if (sectionNode == null) {
			throw new SectionNotFoundException();
		}
		// Insert Security Check

		sectionNode.update(sectionNodeDTO);
		sectionNode = sectionNodeRepository.updateSectionNode(sectionNode);

		return new SectionNodeDTO(sectionNode, this.fliRepo.getSubFiles(sectionNode.getId(), FileType.SECTION));
	}

	public FolderNodeDTO updateFolder(FolderNodeDTO folderNodeDTO, Integer id) {
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}

		if (folderNodeDTO == null || folderNodeDTO.getName() == null || id == null) {
			throw new InvalidDataException();
		}

		FolderNode folderNode = folderNodeRepository.getFolderNode(id);
		if (folderNode == null) {
			throw new SectionNotFoundException();
		}
		// Insert Security Check

		folderNode.update(folderNodeDTO);
		folderNode = folderNodeRepository.updateFolderNode(folderNode);

		return new FolderNodeDTO(folderNode, this.fliRepo.getSubFiles(folderNode.getId(), FileType.FOLDER));
	}

	public void deleteSection(Integer id) {
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}

		if (id == null) {
			throw new InvalidDataException();
		}

		SectionNode sectionNode = sectionNodeRepository.getSectionNode(id);
		if (sectionNode == null) {
			throw new SectionNotFoundException();
		}
		// Insert Security Check

		sectionNodeRepository.removeSectionNode(sectionNode);
		this.deleteFolderCompleteBypassSecurity(sectionNode.getId(), FileType.SECTION);
	}

	public void deleteFolder(Integer id) {
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}

		if (id == null) {
			throw new InvalidDataException();
		}

		FolderNode folderNode = folderNodeRepository.getFolderNode(id);
		if (folderNode == null) {
			throw new SectionNotFoundException();
		}
		// Insert Security Check

		folderNodeRepository.removeFolderNode(folderNode);
		this.deleteFolderCompleteBypassSecurity(folderNode.getId(), FileType.FOLDER);
	}

	public void deleteFolderCompleteBypassSecurity(Integer fileId, FileType fileType) {
		if (fileType != FileType.FILE) {
			ArrayList<FileLocationInfo> subFiles = this.fliRepo.getFileLocationInfoFromHead(fileId);
			for (FileLocationInfo subFile : subFiles) {
				if (subFile.getHeadFileType() == fileType) {
					this.deleteFolderCompleteBypassSecurity(subFile.getTargetFileId(), subFile.getTargetFileType());
				}
			}
		}
		FileLocationInfo fileLocationInfo = this.fliRepo.getFileLocationInfoForFile(fileId, fileType);
		this.fliRepo.removeFileLocationInfo(fileLocationInfo);
		switch(fileType) {
			case FILE:
				this.fileDataService.deleteFileNodeBypassSecurity(this.fileNodeRepository.getFileNode(fileId));
				break;
			case FOLDER:
				this.folderNodeRepository.removeFolderNode(fileId);
				break;
			case SECTION:
				this.sectionNodeRepository.removeSectionNode(fileId);
				break;
		}
	}
}
