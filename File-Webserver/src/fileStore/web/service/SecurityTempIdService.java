package fileStore.web.service;

import authorisation.web.exceptions.NotLoggedinException;
import authorisation.web.service.SecurityService;
import collection.sync.SyncArrayList;
import fileStore.web.model.SecurityTempId;
import userSystem.data.ValidatedUser;

public class SecurityTempIdService {

	private final SyncArrayList<SecurityTempId> tempIds = new SyncArrayList<>();

	private SecurityService securityService;

	public SecurityTempIdService create(SecurityService securityService) {
		this.securityService = securityService;
		return this;
	}

	public String generateTempId() {
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser == null) {
			throw new NotLoggedinException();
		}

		SecurityTempId tempId = new SecurityTempId();
		this.tempIds.add(tempId);
		return tempId.getId();
	}

	public boolean validateTempId(String tempUrl) {
		for (int i = 0; i < this.tempIds.size(); i++) {
			SecurityTempId url = this.tempIds.get(i);
			if (!url.isValid()) {
				this.tempIds.remove(i);
				i--;
			} else if (url.getId().equals(tempUrl)) {
				return true;
			}
		}
		return false;
	}
}
