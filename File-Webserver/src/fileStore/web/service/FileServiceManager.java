package fileStore.web.service;

import authorisation.web.service.AuthorisationServiceManager;

public class FileServiceManager {

	public static final FileServiceManager FILE_SERVICE_MANAGER = new FileServiceManager();

	private FileNodeService fileNodeService;
	private FileDataService fileDataService;
	private FolderService folderService;
	private SecurityTempIdService securityTempIdService;

	private FileServiceManager() {
	}

	public FileServiceManager create(AuthorisationServiceManager authorisationServiceManager) {
		this.folderService = new FolderService();
		this.fileNodeService = new FileNodeService();
		this.fileDataService = new FileDataService();
		this.securityTempIdService = new SecurityTempIdService();

		this.folderService.create(authorisationServiceManager.getSecurityService(), fileDataService);
		this.fileNodeService.create(authorisationServiceManager.getSecurityService());
		this.fileNodeService.create(authorisationServiceManager.getSecurityService());
		this.fileDataService.create(authorisationServiceManager.getSecurityService(), fileNodeService, folderService);
		this.securityTempIdService.create(authorisationServiceManager.getSecurityService());
		return this;
	}

	public FileNodeService getFileNodeService() {
		return fileNodeService;
	}

	public FileDataService getFileDataService() {
		return fileDataService;
	}

	public SecurityTempIdService getSecurityTempIdService() {
		return securityTempIdService;
	}

	public FolderService getFolderService() {
		return folderService;
	}
}
