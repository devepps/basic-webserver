package fileStore.web;

import authorisation.web.AuthorisationWebServer;
import authorisation.web.service.AuthorisationServiceManager;
import fileStore.web.controller.FileDataController;
import fileStore.web.controller.FileNodeController;
import fileStore.web.controller.FolderController;
import fileStore.web.dto.FileNodeDTO;
import fileStore.web.dto.FolderNodeDTO;
import fileStore.web.dto.SectionNodeDTO;
import fileStore.web.service.FileServiceManager;
import server.serverManager.ServerBuilder;

public class FileNodeWebServer {

	private AuthorisationWebServer aws;

	public FileNodeWebServer() {
		aws = new AuthorisationWebServer();
		FileServiceManager.FILE_SERVICE_MANAGER.create(AuthorisationServiceManager.SERVICE_MANAGER);
	}

	public void buildOwn() {
		ServerBuilder sb = new ServerBuilder("fileNode.web.server.port", "fileNode.web.keyStore.path",
				"fileNode.web.keyStore.password");
		this.aws.build(sb);
		this.build(sb);
		sb.build();
	}

	public ServerBuilder build(ServerBuilder serverBuilder) {
		return serverBuilder
				.addJSONType(FileNodeDTO.class)
				.addJSONType(FolderNodeDTO.class)
				.addJSONType(SectionNodeDTO.class)
				.addController(FileDataController.class)
				.addController(FileNodeController.class)
				.addController(FolderController.class);
	}

}
