package fileStore.web.exceptions;

import server.data.exceptions.HTTPException;
import server.data.type.ResponseType;

public class InvalidFileNodeException extends HTTPException {

	private static final long serialVersionUID = 1L;

	public InvalidFileNodeException() {
		super(ResponseType.BAD_REQUEST, "Invalid data. Must provide an id, name and Filetype.");
	}

}
