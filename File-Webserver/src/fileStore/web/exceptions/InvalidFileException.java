package fileStore.web.exceptions;

import server.data.exceptions.HTTPException;
import server.data.type.ResponseType;

public class InvalidFileException extends HTTPException {

	private static final long serialVersionUID = 1L;

	public InvalidFileException() {
		super(ResponseType.BAD_REQUEST, "Invalid data, file could not be saved.");
	}

}
