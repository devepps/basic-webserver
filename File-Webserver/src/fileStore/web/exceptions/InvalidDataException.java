package fileStore.web.exceptions;

import server.data.exceptions.HTTPException;
import server.data.type.ResponseType;

public class InvalidDataException extends HTTPException {

	private static final long serialVersionUID = 1L;

	public InvalidDataException() {
		super(ResponseType.BAD_REQUEST, "Invalid request data");
	}


}
