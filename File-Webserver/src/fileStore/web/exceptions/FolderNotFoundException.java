package fileStore.web.exceptions;

import server.data.exceptions.HTTPException;
import server.data.type.ResponseType;

public class FolderNotFoundException extends HTTPException {

	private static final long serialVersionUID = 1L;

	public FolderNotFoundException() {
		super(ResponseType.BAD_REQUEST, "Requested Section hast not been found");
	}


}
