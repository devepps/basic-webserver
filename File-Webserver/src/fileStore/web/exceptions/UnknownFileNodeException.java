package fileStore.web.exceptions;

import server.data.exceptions.HTTPException;
import server.data.type.ResponseType;

public class UnknownFileNodeException extends HTTPException {

	private static final long serialVersionUID = 1L;

	public UnknownFileNodeException() {
		super(ResponseType.BAD_REQUEST, "No Data found.");
	}

}
