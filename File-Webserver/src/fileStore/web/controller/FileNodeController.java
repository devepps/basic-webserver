package fileStore.web.controller;

import java.util.ArrayList;

import authorisation.web.interceptor.UserSecurity;
import fileStore.repository.types.FileNode;
import fileStore.web.dto.FileNodeDTO;
import fileStore.web.service.FileDataService;
import fileStore.web.service.FileNodeService;
import fileStore.web.service.FileServiceManager;
import server.data.controller.Controller;
import server.data.interceptor.before.Before;
import server.data.parameter.ParameterConfig;
import server.data.requestAcceptor.RequestAcceptor;
import server.data.responseMessage.ResponseMessage;
import server.data.type.ContentType;
import server.data.type.ParameterType;
import server.data.type.RequestType;
import server.data.type.ResponseType;

@Controller(basePath = "nodes/files")
public class FileNodeController {

	private final FileNodeService fileNodeService = FileServiceManager.FILE_SERVICE_MANAGER.getFileNodeService();
	private final FileDataService fileDataService = FileServiceManager.FILE_SERVICE_MANAGER.getFileDataService();

	@RequestAcceptor(path = "own", requestType = RequestType.GET)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage getFileNodes() {

		ArrayList<FileNodeDTO> fileNodes = fileNodeService.getFileNodes();
		Object[] fileNodeArray = new Object[fileNodes.size()];
		fileNodeArray = fileNodes.toArray(fileNodeArray);

		return new ResponseMessage(ResponseType.OK, fileNodeArray);
	}

	@RequestAcceptor(path = "any", requestType = RequestType.POST)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage updateFileNode(
			@ParameterConfig(contentType = ContentType.BASE_TYPE, name = "id", parameterType = ParameterType.URL) Integer id,
			@ParameterConfig(contentType = ContentType.JSON_OBJECT, name = "fileNode", parameterType = ParameterType.BODY) FileNodeDTO fileNode) {

		FileNodeDTO updatedFileNode = fileNodeService.updateFileNode(id, fileNode);

		return new ResponseMessage(ResponseType.OK, updatedFileNode);
	}

	@RequestAcceptor(path = "any", requestType = RequestType.DELETE)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage deleteFileNodeFromFolder(
			@ParameterConfig(contentType = ContentType.BASE_TYPE, name = "id", parameterType = ParameterType.URL) Integer id) {

		this.fileDataService.deleteFileNode(id);

		return new ResponseMessage(ResponseType.OK);
	}

	@RequestAcceptor(path = "any", requestType = RequestType.GET)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage getFileNode(
			@ParameterConfig(contentType = ContentType.BASE_TYPE, name = "id", parameterType = ParameterType.URL) Integer id) {

		FileNode fileNode = this.fileNodeService.getFileNode(id);

		return new ResponseMessage(ResponseType.OK, new FileNodeDTO(fileNode));
	}

}
