package fileStore.web.controller;

import java.io.InputStream;

import authorisation.web.exceptions.NoPermissionException;
import authorisation.web.interceptor.UserSecurity;
import fileStore.repository.types.FileNode;
import fileStore.web.dto.FileNodeDTO;
import fileStore.web.service.FileDataService;
import fileStore.web.service.FileNodeService;
import fileStore.web.service.FileServiceManager;
import fileStore.web.service.SecurityTempIdService;
import server.data.controller.Controller;
import server.data.interceptor.before.Before;
import server.data.parameter.ParameterConfig;
import server.data.requestAcceptor.PureRequestAcceptor;
import server.data.requestAcceptor.RequestAcceptor;
import server.data.responseMessage.PureResponseMessage;
import server.data.responseMessage.ResponseMessage;
import server.data.streams.TransferStream;
import server.data.type.ContentType;
import server.data.type.ParameterType;
import server.data.type.RequestType;
import server.data.type.ResponseType;

@Controller(basePath = "data")
public class FileDataController {

	private final FileDataService fileDataService = FileServiceManager.FILE_SERVICE_MANAGER.getFileDataService();
	private final FileNodeService fileNodeService = FileServiceManager.FILE_SERVICE_MANAGER.getFileNodeService();
	private final SecurityTempIdService securityTempIdService = FileServiceManager.FILE_SERVICE_MANAGER.getSecurityTempIdService();

	@PureRequestAcceptor(path = "link", requestType = RequestType.GET)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage getFileDataLink(@ParameterConfig(contentType = ContentType.BASE_TYPE, name = "id", parameterType = ParameterType.URL) Integer id) {
		fileNodeService.getFileNode(id);
		return new ResponseMessage(ResponseType.OK, securityTempIdService.generateTempId());
	}

	@PureRequestAcceptor(path = "raw", requestType = RequestType.GET)
	public PureResponseMessage getFileData(
			@ParameterConfig(contentType = ContentType.BASE_TYPE, name = "id", parameterType = ParameterType.URL) Integer id,
			@ParameterConfig(contentType = ContentType.BASE_TYPE, name = "securityId", parameterType = ParameterType.URL) String securityId) {
		if (!securityTempIdService.validateTempId(securityId)) {
			throw new NoPermissionException();
		}
		FileNode fileNode = fileNodeService.getFileNodeBypassSecurity(id);
		TransferStream data = fileDataService.getFileData(fileNode);

		return new PureResponseMessage(ResponseType.OK, data, fileNode.getDataType());
	}

	@RequestAcceptor(path = "raw", requestType = RequestType.POST, acceptsRawData = true)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage updateFileData(
			@ParameterConfig(contentType = ContentType.BASE_TYPE, name = "id", parameterType = ParameterType.URL) Integer id,
			@ParameterConfig(contentType = ContentType.BASE_TYPE, name = "data", parameterType = ParameterType.BODY) InputStream data) {

		FileNodeDTO updatedFileNode = fileDataService.updateFileData(data, id);

		return new ResponseMessage(ResponseType.OK, updatedFileNode);
	}

	@RequestAcceptor(path = "raw/folder", requestType = RequestType.PUT, acceptsRawData = true)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage addFileDataToFolder(
			@ParameterConfig(contentType = ContentType.BASE_TYPE, name = "fileName", parameterType = ParameterType.URL) String fileName,
			@ParameterConfig(contentType = ContentType.BASE_TYPE, name = "dataType", parameterType = ParameterType.URL) String dataType,
			@ParameterConfig(contentType = ContentType.BASE_TYPE, name = "folderId", parameterType = ParameterType.URL) Integer folderId,
			@ParameterConfig(contentType = ContentType.BASE_TYPE, name = "data", parameterType = ParameterType.BODY) InputStream data) {

		FileNodeDTO updatedFileNode = fileDataService.addFileData(data, new FileNodeDTO(null, fileName, dataType, null, null), folderId, true);

		return new ResponseMessage(ResponseType.OK, updatedFileNode);
	}

	@RequestAcceptor(path = "raw/section", requestType = RequestType.PUT, acceptsRawData = true)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage addFileDataToSection(
			@ParameterConfig(contentType = ContentType.BASE_TYPE, name = "fileName", parameterType = ParameterType.URL) String fileName,
			@ParameterConfig(contentType = ContentType.BASE_TYPE, name = "dataType", parameterType = ParameterType.URL) String dataType,
			@ParameterConfig(contentType = ContentType.BASE_TYPE, name = "sectionId", parameterType = ParameterType.URL) Integer folderId,
			@ParameterConfig(contentType = ContentType.BASE_TYPE, name = "data", parameterType = ParameterType.BODY) InputStream data) {

		FileNodeDTO updatedFileNode = fileDataService.addFileData(data, new FileNodeDTO(null, fileName, dataType, null, null), folderId, false);

		return new ResponseMessage(ResponseType.OK, updatedFileNode);
	}

}
