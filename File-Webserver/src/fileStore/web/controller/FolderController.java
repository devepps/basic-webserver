package fileStore.web.controller;


import authorisation.web.interceptor.UserSecurity;
import fileStore.web.dto.FolderNodeDTO;
import fileStore.web.dto.SectionNodeDTO;
import fileStore.web.service.FileServiceManager;
import fileStore.web.service.FolderService;
import server.data.controller.Controller;
import server.data.interceptor.before.Before;
import server.data.parameter.ParameterConfig;
import server.data.requestAcceptor.PureRequestAcceptor;
import server.data.responseMessage.ResponseMessage;
import server.data.type.ContentType;
import server.data.type.ParameterType;
import server.data.type.RequestType;
import server.data.type.ResponseType;

@Controller(basePath = "nodes")
public class FolderController {
	
	private final FolderService folderService = FileServiceManager.FILE_SERVICE_MANAGER.getFolderService();

	@PureRequestAcceptor(path = "sections", requestType = RequestType.GET)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage getSections() {
		
		SectionNodeDTO[] sections = folderService.getSections();
		return new ResponseMessage(ResponseType.OK, ((Object[]) sections));
	}

	@PureRequestAcceptor(path = "folders", requestType = RequestType.GET)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage getFolder(@ParameterConfig(name = "id", contentType = ContentType.BASE_TYPE, parameterType = ParameterType.URL) Integer id) {
		
		FolderNodeDTO folder = folderService.getFolder(id);
		return new ResponseMessage(ResponseType.OK, folder);
	}

	@PureRequestAcceptor(path = "sections", requestType = RequestType.PUT)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage addSections(@ParameterConfig(name = "section", contentType = ContentType.JSON_OBJECT) SectionNodeDTO sectionNode) {
		
		SectionNodeDTO section = folderService.addSection(sectionNode);
		return new ResponseMessage(ResponseType.OK, section);
	}

	@PureRequestAcceptor(path = "folders/section", requestType = RequestType.PUT)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage addFolderOnSection(@ParameterConfig(name = "folder", contentType = ContentType.JSON_OBJECT) FolderNodeDTO folderNode,
			@ParameterConfig(name = "section", contentType = ContentType.JSON_OBJECT) SectionNodeDTO sectionNode) {
		
		FolderNodeDTO folder = folderService.addFolder(folderNode, sectionNode);
		return new ResponseMessage(ResponseType.OK, folder);
	}

	@PureRequestAcceptor(path = "folders/folder", requestType = RequestType.PUT)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage addFolderOnFolder(@ParameterConfig(name = "folder", contentType = ContentType.JSON_OBJECT) FolderNodeDTO folderNode,
			@ParameterConfig(name = "headFolder", contentType = ContentType.JSON_OBJECT) FolderNodeDTO headFolderNode) {
		
		FolderNodeDTO folder = folderService.addFolder(folderNode, headFolderNode);
		return new ResponseMessage(ResponseType.OK, folder);
	}

	@PureRequestAcceptor(path = "sections", requestType = RequestType.POST)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage updateSection(@ParameterConfig(name = "section", contentType = ContentType.JSON_OBJECT) SectionNodeDTO sectionNode,
			@ParameterConfig(name = "id", contentType = ContentType.BASE_TYPE, parameterType = ParameterType.URL) Integer id) {
		
		SectionNodeDTO section = folderService.updateSection(sectionNode, id);
		return new ResponseMessage(ResponseType.OK, section);
	}

	@PureRequestAcceptor(path = "folders", requestType = RequestType.POST)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage updateFolder(@ParameterConfig(name = "folder", contentType = ContentType.JSON_OBJECT) FolderNodeDTO folderNode,
			@ParameterConfig(name = "id", contentType = ContentType.BASE_TYPE, parameterType = ParameterType.URL) Integer id) {
		
		FolderNodeDTO folder = folderService.updateFolder(folderNode, id);
		return new ResponseMessage(ResponseType.OK, folder);
	}

	@PureRequestAcceptor(path = "sections", requestType = RequestType.DELETE)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage deleteSection(
			@ParameterConfig(name = "id", contentType = ContentType.BASE_TYPE, parameterType = ParameterType.URL) Integer id) {
		
		folderService.deleteSection(id);
		return new ResponseMessage(ResponseType.OK);
	}

	@PureRequestAcceptor(path = "folders", requestType = RequestType.DELETE)
	@Before(befores = { UserSecurity.class })
	public ResponseMessage delteFolder(
			@ParameterConfig(name = "id", contentType = ContentType.BASE_TYPE, parameterType = ParameterType.URL) Integer id) {
		
		folderService.deleteFolder(id);
		return new ResponseMessage(ResponseType.OK);
	}

}
