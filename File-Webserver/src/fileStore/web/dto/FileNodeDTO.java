package fileStore.web.dto;

import java.time.LocalDateTime;

import fileStore.repository.types.FileNode;
import json.JSON;

@JSON(name = "fileNode")
public class FileNodeDTO {

	private Integer id;
	private String name;
	private String dataType;
	private Integer size;
	private LocalDateTime created;

	public FileNodeDTO(FileNode fileNode) {
		super();
		this.id = fileNode.getId();
		this.name = fileNode.getName();
		this.dataType = fileNode.getDataType();
		this.size = fileNode.getSize();
		this.created = fileNode.getCreated();
	}

	public FileNodeDTO(Integer id, String name, String dataType, Integer size, LocalDateTime created) {
		super();
		this.id = id;
		this.name = name;
		this.dataType = dataType;
		this.size = size;
		this.created = created;
	}

	public FileNodeDTO() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDataType() {
		return dataType;
	}

	public Integer getSize() {
		return size;
	}

	@Override
	public String toString() {
		return "FileNodeDTO [id=" + id + ", name=" + name + ", dataType=" + dataType + ", size=" + size + ", created="
				+ created + "]";
	}

}
