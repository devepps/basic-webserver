package fileStore.web.dto;

import java.time.LocalDateTime;
import java.util.Arrays;

import fileStore.repository.types.SectionNode;
import fileStore.web.model.SubFileList;
import json.JSON;

@JSON(name = "SectionNode")
public class SectionNodeDTO {

	private Integer id;
	private String name;
	private LocalDateTime created;
	private Integer[] fileNodeIds;
	private Integer[] subFolderIds;

	public SectionNodeDTO() {
		super();
	}

	public SectionNodeDTO(Integer id, String name, LocalDateTime created, Integer[] fileNodeIds,
			Integer[] subFolders) {
		super();
		this.id = id;
		this.name = name;
		this.created = created;
		this.fileNodeIds = fileNodeIds;
		this.subFolderIds = subFolders;
	}

	public SectionNodeDTO(SectionNode node, SubFileList subFiles) {
		this.id = node.getId();
		this.name = node.getName();
		this.created = node.getCreated();
		this.fileNodeIds = subFiles.getSubFiles();
		this.subFolderIds = subFiles.getSubFolders();
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public Integer[] getFileNodeIds() {
		return fileNodeIds;
	}

	public Integer[] getSubFolders() {
		return subFolderIds;
	}

	@Override
	public String toString() {
		return "FolderNodeDTO [id=" + id + ", name=" + name + ", created=" + created
				+ ", fileNodeIds=" + Arrays.toString(fileNodeIds) + ", subFolders=" + Arrays.toString(subFolderIds) + "]";
	}

}
