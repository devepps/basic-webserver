package fileStore.java;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import config.ConfigEnvironment;
import fileStore.repository.types.FileNode;
import server.data.streams.TransferStream;

public class FileStore {

	private static final String basePath = ConfigEnvironment.getProperty("fileStore.basePath");

	public FileStore() {

	}

	public long addFile(InputStream in, FileNode fileNode) {
		String path = generatePath(fileNode);
		if (hasFileAtPath(path)) {
			return -1;
		}

		boolean folderExists = createDirectory(fileNode);
		if (!folderExists) return -1;

		File f = new File(path);
		try {
			f.createNewFile();
			GZIPOutputStream out = new GZIPOutputStream(new FileOutputStream(f, false));
			byte[] data = new byte[1024 * 1024];
			long length = 0;
			for (int read = in.read(data); read > 0; read = in.read(data)) {
				out.write(data, 0, read);
				length += read;
			}
			in.close();
			out.close();
			return length;
		} catch (IOException e) {
			return -1;
		}
	}

	public long updateFile(InputStream in, FileNode fileNode) {
		String path = generatePath(fileNode);
		if (!hasFileAtPath(path)) {
			return addFile(in, fileNode);
		}

		try {
			GZIPOutputStream out = new GZIPOutputStream(new FileOutputStream(new File(path), false));
			byte[] data = new byte[1024 * 1024];
			long length = 0;
			for (int read = in.read(data); read > 0; read = in.read(data)) {
				out.write(data, 0, read);
				length += read;
			}
			in.close();
			out.close();
			return length;
		} catch (IOException e) {
			return -1;
		}
	}

	public boolean removeFile(FileNode fileNode) {
		String path = generatePath(fileNode);
		if (!hasFileAtPath(path)) {
			return true;
		}

		File f = new File(path);
		return f.delete();
	}

	public TransferStream getFile(FileNode fileNode) {
		String path = generatePath(fileNode);
		if (!hasFileAtPath(path)) {
			return null;
		}

		try {
			File f = new File(path);
			return new TransferStream(new GZIPInputStream(new FileInputStream(f)), fileNode.getSize());
		} catch (IOException e) {
			return null;
		}
	}

	private boolean createDirectory(FileNode fileNode) {
		File f = new File(basePath + "/" + fileNode.getUserId());
		if (!f.exists() || !f.isDirectory()) {
			return f.mkdirs();
		}
		return true;
	}

	private boolean hasFileAtPath(String path) {
		File f = new File(path);
		return f.exists() && f.isFile() && f.canRead() && f.canWrite();
	}

	private String generatePath(FileNode fileNode) {
		return basePath + "/" + fileNode.getUserId() + "/" + fileNode.getId() + ".gz";
	}

}
