package fileStore;

import fileStore.repository.FSAIdRepository;
import fileStore.repository.FSAUserRepository;
import fileStore.repository.FileLocationInfoRepository;
import fileStore.repository.FileNodeRepository;
import fileStore.repository.FolderNodeRepository;
import fileStore.repository.SectionNodeRepository;
import fileStore.repository.data.SCProtocolExtension;
import fileStore.web.FileNodeWebServer;

public class FileStoreServer {

	public static void main(String[] args) {
		new FileStoreServer();
	}

	public FileStoreServer() {
		new SCProtocolExtension();
		new FileNodeRepository();
		new FolderNodeRepository();
		new SectionNodeRepository();
		new FSAIdRepository();
		new FSAUserRepository();
		new FileLocationInfoRepository();
		new FileNodeWebServer().buildOwn();
	}

}
