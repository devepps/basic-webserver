package fileStore.repository;

import java.util.ArrayList;

import collection.tick.TickManager;
import fileStore.repository.types.FileSecureAccessUser;
import repositoryClient.client.RepoClientInterface;
import repositoryClient.data.Repository;
import userSystem.data.ValidatedUser;

public class FSAUserRepository {

	public static FSAUserRepository FSA_USER_REPOSITORY;

	private Repository<FileSecureAccessUser> repository;

	private RepoClientInterface repoClient;

	public FSAUserRepository() {
		this.repoClient = new RepoClientInterface("Repository.Client.Connection.FSA.User.Ip",
				"Repository.Client.Connection.FSA.User.Port", "Repository.Client.Connection.FSA.User.keyStorePath");

		this.repository = repoClient.createRepository(FileSecureAccessUser.class);

		new TickManager(() -> repoClient.tick(), 1);

		FSA_USER_REPOSITORY = this;
	}

	public ArrayList<FileSecureAccessUser> getFSAUsers(ValidatedUser user) {
		return this.repository.getByData(user.getUser().getId(), "accessUser");
	}

	public void removeFSAUser(Integer id) {
		this.repository.removeByKey(id);
	}

	public void removeFSAUser(FileSecureAccessUser FSAUser) {
		this.repository.removeByKey(FSAUser.getId());
	}

	public FileSecureAccessUser getFileNode(Integer id) {
		return this.repository.getByKey(id);
	}

	public FileSecureAccessUser addFileNode(FileSecureAccessUser FSAUser) {
		return this.repository.add(FSAUser);
	}

	public FileSecureAccessUser updateFileNode(FileSecureAccessUser FSAUser) {
		return this.repository.set(FSAUser);
	}

}
