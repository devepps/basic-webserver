package fileStore.repository;

public enum FileType {

	FILE(1), FOLDER(2), SECTION(3);

	private int code;

	private FileType(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public static FileType parse(Integer code) {
		for (FileType type : FileType.values()) {
			if (type.code == code)
				return type;
		}
		return null;
	}
}
