package fileStore.repository;

import java.util.ArrayList;

import collection.tick.TickManager;
import fileStore.repository.types.SectionNode;
import repositoryClient.client.RepoClientInterface;
import repositoryClient.data.Repository;
import userSystem.data.ValidatedUser;

public class SectionNodeRepository {

	public static SectionNodeRepository SECTION_NODE_REPOSITORY;

	private Repository<SectionNode> repository;

	private RepoClientInterface repoClient;

	public SectionNodeRepository() {
		this.repoClient = new RepoClientInterface("Repository.Client.Connection.SectionNode.Ip",
				"Repository.Client.Connection.SectionNode.Port",
				"Repository.Client.Connection.SectionNode.keyStorePath");

		this.repository = repoClient.createRepository(SectionNode.class);

		new TickManager(() -> repoClient.tick(), 1);

		SECTION_NODE_REPOSITORY = this;
	}

	public ArrayList<SectionNode> getSectionNodes(ValidatedUser user) {
		return this.repository.getByData(user.getUser().getId(), "userId");
	}

	public void removeSectionNode(Integer id) {
		this.repository.removeByKey(id);
	}

	public void removeSectionNode(SectionNode sectionNode) {
		this.repository.removeByKey(sectionNode.getId());
	}

	public SectionNode getSectionNode(Integer id) {
		return this.repository.getByKey(id);
	}

	public SectionNode addSectionNode(SectionNode sectionNode) {
		return this.repository.add(sectionNode);
	}

	public SectionNode updateSectionNode(SectionNode sectionNode) {
		return this.repository.set(sectionNode);
	}

}
