package fileStore.repository;

import java.util.ArrayList;
import java.util.Arrays;

import collection.tick.TickManager;
import fileStore.logger.FileServerLogger;
import fileStore.repository.types.FileLocationInfo;
import fileStore.web.model.SubFileList;
import repositoryClient.client.RepoClientInterface;
import repositoryClient.data.Repository;

public class FileLocationInfoRepository {

	public static FileLocationInfoRepository FILE_LOCATION_INFO_REPOSITORY;

	private Repository<FileLocationInfo> repository;

	private RepoClientInterface repoClient;

	public FileLocationInfoRepository() {
		this.repoClient = new RepoClientInterface("Repository.Client.Connection.FileLocationInfo.Ip",
				"Repository.Client.Connection.FileLocationInfo.Port",
				"Repository.Client.Connection.FileLocationInfo.keyStorePath");

		this.repository = repoClient.createRepository(FileLocationInfo.class);

		new TickManager(() -> repoClient.tick(), 1);

		FILE_LOCATION_INFO_REPOSITORY = this;
	}

	public ArrayList<FileLocationInfo> getFileLocationInfoFromTarget(Integer id) {
		return this.repository.getByData(id, "targetFileId");
	}

	public ArrayList<FileLocationInfo> getFileLocationInfoFromHead(Integer id) {
		return this.repository.getByData(id, "headFileId");
	}

	public void removeFileLocationInfo(Integer id) {
		this.repository.removeByKey(id);
	}

	public void removeFileLocationInfo(FileLocationInfo info) {
		this.repository.removeByKey(info.getId());
	}

	public FileLocationInfo getFileLocationInfo(Integer id) {
		return this.repository.getByKey(id);
	}

	public FileLocationInfo addFileLocationInfo(FileLocationInfo info) {
		return this.repository.add(info);
	}

	public FileLocationInfo updateFileLocationInfo(FileLocationInfo info) {
		return this.repository.set(info);
	}

	public SubFileList getSubFiles(Integer headFileId, FileType headFileType) {
		ArrayList<FileLocationInfo> subFiles = this.getFileLocationInfoFromHead(headFileId);
		Integer[] files = new Integer[subFiles.size()];
		Integer[] folders = new Integer[subFiles.size()];
		int fileIndex = 0;
		int folderIndex = 0;
		for (FileLocationInfo info : subFiles) {
			if (info.getTargetFileType() == FileType.FILE) {
				files[fileIndex] = info.getTargetFileId();
				fileIndex++;
			} else if (info.getTargetFileType() == FileType.FOLDER) {
				folders[folderIndex] = info.getTargetFileId();
				folderIndex++;
			}
		}
		return new SubFileList(Arrays.copyOfRange(files, 0, fileIndex), Arrays.copyOfRange(folders, 0, folderIndex));
	}

	public FileLocationInfo getFileLocationInfoForFile(Integer fileId, FileType fileType) {
		ArrayList<FileLocationInfo> possibleFileLocationInfos = this.getFileLocationInfoFromTarget(fileId);
		FileServerLogger.debug("found target location for \"" + fileId + "\" : \"" +possibleFileLocationInfos + " \"" );
		for (FileLocationInfo pflInfo : possibleFileLocationInfos) {
			if (pflInfo.getTargetFileType() == fileType) {
				return pflInfo;
			}
		}
		return null;
	}

}
