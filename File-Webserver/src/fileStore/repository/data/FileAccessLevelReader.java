package fileStore.repository.data;

import java.nio.ByteBuffer;

import baseSC.data.dataReader.SingleDataReader;
import fileStore.web.security.FileAccessLevel;

public class FileAccessLevelReader extends SingleDataReader {

	public FileAccessLevelReader() {
		super(SCProtocolExtension.FILE_ACCESS_LEVEL);
		System.out.println("FA-Reader created");
	}

	@Override
	public byte[] readData(Object readableContent) {
		System.out.println("FA-Reader toBytes: " + readableContent);
		return ByteBuffer.allocate(Integer.BYTES).putInt(((FileAccessLevel) readableContent).getAccessLevel()).array();
	}

	@Override
	public Object readData(byte[] readableData) {
		System.out.println("FA-Reader fromBytes: " + readableData);
		if (readableData.length == Integer.BYTES) {
			return FileAccessLevel.parse(ByteBuffer.wrap(readableData).getInt());
		} else {
			return FileAccessLevel.NO_ACCESS;
		}
	}

	@Override
	public String toString() {
		return "FileAccessLevelReader []";
	}

}
