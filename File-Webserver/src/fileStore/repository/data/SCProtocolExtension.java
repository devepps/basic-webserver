package fileStore.repository.data;

import baseSC.data.types.SingleDataReaderType;

public class SCProtocolExtension {

	public static final String KEY_FILE_ACCESS_LEVEL = "Connection.Reader-ID.fileSecurity.File-Access-Level:0";
	public static final String KEY_FILE_TYPE = "Connection.Reader-ID.fileSecurity.File-Type:0";
	
	public static final SingleDataReaderType FILE_ACCESS_LEVEL = new SingleDataReaderType(KEY_FILE_ACCESS_LEVEL, new FileAccessLevelReader());
	public static final SingleDataReaderType FILE_TYPE = new SingleDataReaderType(KEY_FILE_TYPE, new FileTypeReader());
	
	public SCProtocolExtension() {
	}

}
