package fileStore.repository.data;

import java.nio.ByteBuffer;

import baseSC.data.dataReader.SingleDataReader;
import fileStore.repository.FileType;

public class FileTypeReader extends SingleDataReader {

	public FileTypeReader() {
		super(SCProtocolExtension.FILE_TYPE);
	}

	@Override
	public byte[] readData(Object readableContent) {
		return ByteBuffer.allocate(Integer.BYTES).putInt(((FileType) readableContent).getCode()).array();
	}

	@Override
	public Object readData(byte[] readableData) {
		if (readableData.length == Integer.BYTES) {
			return FileType.parse(ByteBuffer.wrap(readableData).getInt());
		} else {
			return null;
		}
	}

	@Override
	public String toString() {
		return "FileAccessLevelReader []";
	}

}
