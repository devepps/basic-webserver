package fileStore.repository;

import java.util.ArrayList;

import collection.tick.TickManager;
import fileStore.repository.types.FileNode;
import repositoryClient.client.RepoClientInterface;
import repositoryClient.data.Repository;
import userSystem.data.ValidatedUser;

public class FileNodeRepository {

	public static FileNodeRepository FILE_NODE_REPOSITORY;

	private Repository<FileNode> fileNodeRepository;

	private RepoClientInterface repoClient;

	public FileNodeRepository() {
		this.repoClient = new RepoClientInterface("Repository.Client.Connection.FileNode.Ip",
				"Repository.Client.Connection.FileNode.Port", "Repository.Client.Connection.FileNode.keyStorePath");

		this.fileNodeRepository = repoClient.createRepository(FileNode.class);

		new TickManager(() -> repoClient.tick(), 1);

		FILE_NODE_REPOSITORY = this;
	}

	public ArrayList<FileNode> getFileNodes(ValidatedUser user) {
		return this.fileNodeRepository.getByData(user.getUser().getId(), "userId");
	}

	public void removeFileNode(Integer id) {
		this.fileNodeRepository.removeByKey(id);
	}

	public FileNode getFileNode(long id) {
		return this.fileNodeRepository.getByKey(id);
	}

	public FileNode addFileNode(FileNode fileNode) {
		return this.fileNodeRepository.add(fileNode);
	}

	public FileNode updateFileNode(FileNode fileNode) {
		return this.fileNodeRepository.set(fileNode);
	}

}
