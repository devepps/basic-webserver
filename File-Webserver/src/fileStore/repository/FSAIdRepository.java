package fileStore.repository;

import java.util.ArrayList;

import collection.tick.TickManager;
import fileStore.repository.types.FileSecureAccessId;
import repositoryClient.client.RepoClientInterface;
import repositoryClient.data.Repository;

public class FSAIdRepository {

	public static FSAIdRepository FSA_ID_REPOSITORY;

	private Repository<FileSecureAccessId> repository;

	private RepoClientInterface repoClient;

	public FSAIdRepository() {
		this.repoClient = new RepoClientInterface("Repository.Client.Connection.FSA.Id.Ip",
				"Repository.Client.Connection.FSA.Id.Port", "Repository.Client.Connection.FSA.Id.keyStorePath");

		this.repository = repoClient.createRepository(FileSecureAccessId.class);

		new TickManager(() -> repoClient.tick(), 1);

		FSA_ID_REPOSITORY = this;
	}

	public ArrayList<FileSecureAccessId> getFSAIds(String accessId) {
		return this.repository.getByData(accessId, "accessId");
	}

	public void removeFSAId(Integer id) {
		this.repository.removeByKey(id);
	}

	public void removeFSAId(FileSecureAccessId fsaID) {
		this.repository.removeByKey(fsaID.getId());
	}

	public FileSecureAccessId getFileNode(Integer id) {
		return this.repository.getByKey(id);
	}

	public FileSecureAccessId addFileNode(FileSecureAccessId fsaID) {
		return this.repository.add(fsaID);
	}

	public FileSecureAccessId updateFileNode(FileSecureAccessId fsaID) {
		return this.repository.set(fsaID);
	}

}
