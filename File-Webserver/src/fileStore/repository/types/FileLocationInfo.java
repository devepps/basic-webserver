package fileStore.repository.types;

import java.io.Serializable;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.types.SingleDataReaderType;
import fileStore.repository.FileType;
import fileStore.repository.data.SCProtocolExtension;
import repository.data.entity.DatabaseEntity;
import repository.data.entity.DatabaseEntry;
import repository.data.entity.EntityContentType;
import repository.data.entity.EntryType;
import repository.data.generator.IntegerGenerator;

@PackagableEntity(configKey = "Connection.DTO-Ids.FileServer.FileLocationInfo")
@DatabaseEntity(name = "FileLocationInfo")
public class FileLocationInfo implements DTO, Serializable {

	private static final long serialVersionUID = 5469429321880295947L;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_INTEGER)
	@DatabaseEntry(type = EntryType.KEY, contentType = EntityContentType.UNIQUE_GENERATED, generator = IntegerGenerator.class)
	private Integer id;
	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_INTEGER)
	@DatabaseEntry(type = EntryType.DATA)
	private Integer targetFileId;
	@PackagableContent(contentTypeKey = SCProtocolExtension.KEY_FILE_TYPE)
	@DatabaseEntry(type = EntryType.DATA)
	private FileType targetFileType;
	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_INTEGER)
	@DatabaseEntry(type = EntryType.DATA)
	private Integer headFileId;
	@PackagableContent(contentTypeKey = SCProtocolExtension.KEY_FILE_TYPE)
	@DatabaseEntry(type = EntryType.DATA)
	private FileType headFileType;
	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_UNICODE)
	@DatabaseEntry(type = EntryType.DATA)
	private String path;

	public FileLocationInfo() {
		super();
	}

	public FileLocationInfo(String path, Integer id, Integer targetFileId, FileType targetFileType, Integer headFileId,
			FileType headFileType) {
		super();
		this.path = path;
		this.id = id;
		this.targetFileId = targetFileId;
		this.targetFileType = targetFileType;
		this.headFileId = headFileId;
		this.headFileType = headFileType;
	}

	public FileLocationInfo(String path, Integer id, Object targetFile, Object headFile) {
		super();
		this.id = id;
		this.path = path;
		if (targetFile instanceof FileNode) {
			this.targetFileId = ((FileNode) targetFile).getId();
			this.targetFileType = FileType.FILE;
		} else if (targetFile instanceof FolderNode) {
			this.targetFileId = ((FolderNode) targetFile).getId();
			this.targetFileType = FileType.FOLDER;
		} else if (targetFile instanceof SectionNode) {
			this.targetFileId = ((SectionNode) targetFile).getId();
			this.targetFileType = FileType.SECTION;
		}
		if (headFile instanceof FileNode) {
			this.headFileId = ((FileNode) headFile).getId();
			this.headFileType = FileType.FILE;
		} else if (headFile instanceof FolderNode) {
			this.headFileId = ((FolderNode) headFile).getId();
			this.headFileType = FileType.FOLDER;
		} else if (headFile instanceof SectionNode) {
			this.headFileId = ((SectionNode) headFile).getId();
			this.headFileType = FileType.SECTION;
		}
	}

	public Integer getId() {
		return id;
	}

	public Integer getTargetFileId() {
		return targetFileId;
	}

	public FileType getTargetFileType() {
		return targetFileType;
	}

	public Integer getHeadFileId() {
		return headFileId;
	}

	public FileType getHeadFileType() {
		return headFileType;
	}

	public String getPath() {
		return path;
	}

	@Override
	public String toString() {
		return "FileLocationInfo [id=" + id + ", targetFileId=" + targetFileId + ", targetFileType=" + targetFileType
				+ ", headFileId=" + headFileId + ", headFileType=" + headFileType + ", path=" + path + "]";
	}

	@Override
	public DTO copy() {
		return new FileLocationInfo(path, id, targetFileId, targetFileType, headFileId, headFileType);
	}

}