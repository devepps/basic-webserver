package fileStore.repository.types;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.types.DefaultValue;
import baseSC.data.types.SingleDataReaderType;
import fileStore.web.dto.FileNodeDTO;
import repository.data.entity.DatabaseEntity;
import repository.data.entity.DatabaseEntry;
import repository.data.entity.EntityContentType;
import repository.data.entity.EntryType;
import repository.data.generator.IntegerGenerator;
import repository.data.generator.LocalDateTimeNowGenerator;
import userSystem.data.ValidatedUser;

@PackagableEntity(configKey = "Connection.DTO-Ids.FileServer.FileNode")
@DatabaseEntity(name = "FileNode")
public class FileNode implements DTO, Serializable {

	private static final long serialVersionUID = -3110598249064891719L;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_INTEGER, defaultValueId = DefaultValue.ID_INTEGER_0)
	@DatabaseEntry(type = EntryType.KEY, contentType = EntityContentType.UNIQUE_GENERATED, generator = IntegerGenerator.class)
	private Integer id;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_ISO_8859_1, defaultValueId = DefaultValue.ID_NULL)
	@DatabaseEntry(type = EntryType.DATA)
	private String name;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_ISO_8859_1, defaultValueId = DefaultValue.ID_NULL)
	@DatabaseEntry(type = EntryType.DATA)
	private String dataType;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_INTEGER, defaultValueId = DefaultValue.ID_INTEGER_0)
	@DatabaseEntry(type = EntryType.DATA)
	private Integer size = 0;

	@PackagableContent(contentTypeKey = SingleDataReaderType.Key_UUID, defaultValueId = DefaultValue.ID_NULL)
	@DatabaseEntry(type = EntryType.DATA)
	private UUID userId;

	@PackagableContent(contentTypeKey = SingleDataReaderType.Key_DATE_TIME, defaultValueId = DefaultValue.ID_NULL)
	@DatabaseEntry(type = EntryType.DATA, generator = LocalDateTimeNowGenerator.class, contentType = EntityContentType.GENERATED)
	private LocalDateTime created;

	public FileNode(Integer id, String name, String dataType, Integer size, UUID userId, LocalDateTime created) {
		super();
		this.id = id;
		this.name = name;
		this.dataType = dataType;
		this.size = size;
		this.userId = userId;
		this.created = created;
	}

	public FileNode(FileNodeDTO fileNode, ValidatedUser validatedUser) {
		super();
		this.id = fileNode.getId();
		this.name = fileNode.getName();
		this.dataType = fileNode.getDataType();
		this.size = fileNode.getSize();
		this.userId = validatedUser.getUser().getId();
	}

	public FileNode() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDataType() {
		return dataType;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public UUID getUserId() {
		return userId;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void update(FileNodeDTO fileNodeDTO) {
		this.name = fileNodeDTO.getName();
		this.dataType = fileNodeDTO.getDataType();
		this.size = fileNodeDTO.getSize();
	}

	@Override
	public String toString() {
		return "FileNode [id=" + id + ", name=" + name + ", dataType=" + dataType + ", size=" + size + ", userId="
				+ userId + ", created=" + created + "]";
	}

	@Override
	public DTO copy() {
		return new FileNode(id, name, dataType, size, userId, created);
	}
}
