package fileStore.repository.types;

import java.time.LocalDateTime;
import java.util.UUID;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.types.SingleDataReaderType;
import fileStore.repository.FileType;
import fileStore.repository.data.SCProtocolExtension;
import fileStore.web.security.FileAccessLevel;
import permission.SecureAccessID;
import repository.data.entity.DatabaseEntity;
import repository.data.entity.DatabaseEntry;
import repository.data.entity.EntryType;

@PackagableEntity(configKey = "Connection.DTO-Ids.FSA.Id")
@DatabaseEntity(name = "FileSecureAccessId")
public class FileSecureAccessId extends SecureAccessID {

	private static final long serialVersionUID = 5469429321880295947L;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_INTEGER)
	@DatabaseEntry(type = EntryType.DATA)
	private Integer targetId;
	@PackagableContent(contentTypeKey = SCProtocolExtension.KEY_FILE_ACCESS_LEVEL)
	@DatabaseEntry(type = EntryType.DATA)
	private FileAccessLevel fileAccessLevel;
	@PackagableContent(contentTypeKey = SCProtocolExtension.KEY_FILE_ACCESS_LEVEL)
	@DatabaseEntry(type = EntryType.DATA)
	private FileType fileType;

	public FileSecureAccessId() {
		super();
	}

	public FileSecureAccessId(Integer id, UUID author, String accessId, LocalDateTime created,
			LocalDateTime expiration, Integer targetId, FileAccessLevel fileAccessLevel, FileType fileType) {
		super(id, author, accessId, created, expiration);
		this.targetId = targetId;
		this.fileAccessLevel = fileAccessLevel;
		this.fileType = fileType;
	}

	public Integer getTargetId() {
		return targetId;
	}

	public FileAccessLevel getFileAccessLevel() {
		return fileAccessLevel;
	}

	public FileType getFileType() {
		return fileType;
	}

	@Override
	public DTO copy() {
		return new FileSecureAccessId(this.getId(), this.getAuthor(), this.getAccessId(), this.getCreated(),
				this.getExpiration(), this.targetId, this.fileAccessLevel, this.fileType);
	}

	public boolean isExpired() {
		return LocalDateTime.now().isAfter(this.getExpiration());
	}

}
