package fileStore.repository.types;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import baseSC.data.DTO;
import baseSC.data.packageType.PackagableContent;
import baseSC.data.packageType.PackagableEntity;
import baseSC.data.types.DefaultValue;
import baseSC.data.types.SingleDataReaderType;
import fileStore.web.dto.FolderNodeDTO;
import repository.data.entity.DatabaseEntity;
import repository.data.entity.DatabaseEntry;
import repository.data.entity.EntityContentType;
import repository.data.entity.EntryType;
import repository.data.generator.IntegerGenerator;
import repository.data.generator.LocalDateTimeNowGenerator;

@PackagableEntity(configKey = "Connection.DTO-Ids.FileServer.FolderNode")
@DatabaseEntity(name = "FolderNode")
public class FolderNode implements DTO, Serializable {

	private static final long serialVersionUID = 2850363681682862338L;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_INTEGER, defaultValueId = DefaultValue.ID_INTEGER_0)
	@DatabaseEntry(type = EntryType.KEY, contentType = EntityContentType.UNIQUE_GENERATED, generator = IntegerGenerator.class)
	private Integer id;

	@PackagableContent(contentTypeKey = SingleDataReaderType.KEY_STRING_ISO_8859_1, defaultValueId = DefaultValue.ID_NULL)
	@DatabaseEntry(type = EntryType.DATA)
	private String name;

	@PackagableContent(contentTypeKey = SingleDataReaderType.Key_UUID, defaultValueId = DefaultValue.ID_NULL)
	@DatabaseEntry(type = EntryType.DATA)
	private UUID userId;

	@PackagableContent(contentTypeKey = SingleDataReaderType.Key_DATE_TIME, defaultValueId = DefaultValue.ID_NULL)
	@DatabaseEntry(type = EntryType.DATA, generator = LocalDateTimeNowGenerator.class, contentType = EntityContentType.GENERATED)
	private LocalDateTime created;

	public FolderNode() {
		super();
	}

	public FolderNode(Integer id, String name, UUID userId, LocalDateTime created) {
		super();
		this.id = id;
		this.name = name;
		this.userId = userId;
		this.created = created;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public UUID getUserId() {
		return userId;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	@Override
	public String toString() {
		return "FolderNode [id=" + id + ", name=" + name + ", userId=" + userId + ", created=" + created + "]";
	}

	@Override
	public DTO copy() {
		return new FolderNode(id, name, userId, created);
	}

	public void update(FolderNodeDTO folderNodeDTO) {
		this.name = folderNodeDTO.getName();
	}

}