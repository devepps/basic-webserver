package fileStore.repository;

import collection.tick.TickManager;
import fileStore.repository.types.FolderNode;
import repositoryClient.client.RepoClientInterface;
import repositoryClient.data.Repository;

public class FolderNodeRepository {

	public static FolderNodeRepository FOLDER_NODE_REPOSITORY;

	private Repository<FolderNode> repository;

	private RepoClientInterface repoClient;

	public FolderNodeRepository() {
		this.repoClient = new RepoClientInterface("Repository.Client.Connection.FolderNode.Ip",
				"Repository.Client.Connection.FolderNode.Port", "Repository.Client.Connection.FolderNode.keyStorePath");

		this.repository = repoClient.createRepository(FolderNode.class);

		new TickManager(() -> repoClient.tick(), 1);

		FOLDER_NODE_REPOSITORY = this;
	}

	public void removeFolderNode(Integer id) {
		this.repository.removeByKey(id);
	}

	public void removeFolderNode(FolderNode folderNode) {
		this.repository.removeByKey(folderNode.getId());
	}

	public FolderNode getFolderNode(Integer id) {
		return this.repository.getByKey(id);
	}

	public FolderNode addFolderNode(FolderNode folderNode) {
		return this.repository.add(folderNode);
	}

	public FolderNode updateFolderNode(FolderNode folderNode) {
		return this.repository.set(folderNode);
	}

}
