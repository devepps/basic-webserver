package authorisation;

import authorisation.web.AuthorisationWebServer;

public class AuthorisationServer {

	public static void main(String[] args) {
		new AuthorisationServer();
	}

	public AuthorisationServer() {
		new AuthorisationWebServer().buildOwn();
	}

}
