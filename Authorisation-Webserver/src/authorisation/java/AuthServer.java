package authorisation.java;

import java.util.UUID;

import authorisation.web.dto.LoginDTO;
import authorisation.web.dto.ValidationDTO;
import server.serverManager.ServerManager;
import userSystem.creation.UserCreation;
import userSystem.data.User;
import userSystem.data.ValidatedUser;
import userSystem.login.UserLogin;
import userSystem.validation.UserValidation;

public class AuthServer {
	
	public static AuthServer AUTH_SERVER;

	private UserLogin userLogin;
	private UserCreation userCreation;
	private UserValidation userValidation;
	
	public AuthServer() {
		AUTH_SERVER = this;

		userLogin = new UserLogin();
		userLogin.login(User.DEFAULT_SYSTEM_USER.getName(), User.DEFAULT_SYSTEM_USER.getPassword());

		userCreation = new UserCreation();
		userCreation.login(User.DEFAULT_SYSTEM_USER.getName(), User.DEFAULT_SYSTEM_USER.getPassword());

		userValidation = new UserValidation();
		userValidation.login(User.DEFAULT_SYSTEM_USER.getName(), User.DEFAULT_SYSTEM_USER.getPassword());
	}

	public ValidatedUser login(LoginDTO login) {
		String ip = ServerManager.SERVER_SERVICE.getIP();
		return this.userLogin.puppetLogin(login.getUsername(), login.getPassword(), ip);
	}

	public boolean createUser(authorisation.web.dto.UserDTO user) {
		return this.userCreation.createUser(user.getUsername(), user.getPassword(), user.getEMail());
	}

	public ValidatedUser validateUser(ValidationDTO validation) {
		String ip = ServerManager.SERVER_SERVICE.getIP();
		return this.userValidation.validateUser(UUID.fromString(validation.getUserId()), validation.getValidationKey(), ip);
	}
}
