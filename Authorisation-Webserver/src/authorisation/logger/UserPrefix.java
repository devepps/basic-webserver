package authorisation.logger;

import authorisation.web.service.SecurityService;
import authorisation.web.service.AuthorisationServiceManager;
import logger.main.prefixs.Prefix;
import userSystem.data.ValidatedUser;

public class UserPrefix extends Prefix {

	@Override
	public String getPrefix() {
		SecurityService securityService = AuthorisationServiceManager.SERVICE_MANAGER.getSecurityService();
		if (securityService == null) {
			return "System-Loading";
		}
		ValidatedUser currentUser = securityService.currentUser();
		if (currentUser != null) {
			return currentUser.getUser().getName();
		}
		return "Unknown";
	}

}
