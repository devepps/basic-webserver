package authorisation.web;

import authorisation.java.AuthServer;
import authorisation.web.controller.UserController;
import authorisation.web.dto.*;
import authorisation.web.service.AuthorisationServiceManager;
import server.serverManager.ServerBuilder;

public class AuthorisationWebServer {

	public AuthorisationWebServer() {
		new AuthServer();
		AuthorisationServiceManager.SERVICE_MANAGER.create();
	}

	public void buildOwn() {
		build(new ServerBuilder("login.web.server.port", "login.web.keyStore.path", "login.web.keyStore.password"))
				.build();
	}

	public ServerBuilder build(ServerBuilder serverBuilder) {
		return serverBuilder.addJSONType(LoginDTO.class).addJSONType(ValidationDTO.class).addJSONType(UserDTO.class)
				.addJSONType(ValidatedUserDTO.class).addController(UserController.class);
	}

}
