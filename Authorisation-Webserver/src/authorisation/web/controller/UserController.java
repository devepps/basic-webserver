package authorisation.web.controller;

import authorisation.web.dto.LoginDTO;
import authorisation.web.dto.UserDTO;
import authorisation.web.dto.ValidatedUserDTO;
import authorisation.web.dto.ValidationDTO;
import authorisation.web.service.LoginService;
import authorisation.web.service.AuthorisationServiceManager;
import server.data.controller.Controller;
import server.data.parameter.ParameterConfig;
import server.data.requestAcceptor.RequestAcceptor;
import server.data.responseMessage.ResponseMessage;
import server.data.type.ContentType;
import server.data.type.RequestType;
import server.data.type.ResponseType;

@Controller(basePath = "user")
public class UserController {

	private LoginService loginService = AuthorisationServiceManager.SERVICE_MANAGER.getLoginService();

	@RequestAcceptor(path = "login", requestType = RequestType.POST)
	public ResponseMessage login(@ParameterConfig(name = "login", contentType = ContentType.JSON_OBJECT) LoginDTO login) {
		ValidationDTO user = loginService.loginUser(login);
		return new ResponseMessage(ResponseType.OK, user);
	}

	@RequestAcceptor(path = "create", requestType = RequestType.PUT)
	public ResponseMessage createUser(@ParameterConfig(name = "user", contentType = ContentType.JSON_OBJECT) UserDTO user) {
		ValidationDTO validatedUser = loginService.createUser(user);
		return new ResponseMessage(ResponseType.OK, validatedUser);
	}

	@RequestAcceptor(path = "validation", requestType = RequestType.POST)
	public ResponseMessage validate(@ParameterConfig(name = "validation", contentType = ContentType.JSON_OBJECT) ValidationDTO validation) {
		ValidatedUserDTO validatedUser = loginService.validateUser(validation);
		return new ResponseMessage(ResponseType.OK, validatedUser);
	}
}
