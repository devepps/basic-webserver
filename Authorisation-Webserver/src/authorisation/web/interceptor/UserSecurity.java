package authorisation.web.interceptor;

import static authorisation.web.service.AuthorisationServiceManager.SERVICE_MANAGER;

import java.util.List;
import java.util.UUID;

import com.sun.net.httpserver.HttpExchange;

import authorisation.logger.AuthorisationServerLogger;
import authorisation.web.service.SecurityService;
import server.data.interceptor.before.BeforeWorker;
import server.data.internalDTO.ErrorDTO;
import server.data.responseMessage.ResponseMessage;
import server.data.type.ResponseType;
import userSystem.data.ValidatedUser;

public class UserSecurity implements BeforeWorker {

	public static final String HEADERS_USERID = "Authorisation-User-Id";
	public static final String HEADERS_VALIDATION_KEY = "Authorisation-User-ValidationKey";

	public UserSecurity() {
	}

	@Override
	public ResponseMessage before(HttpExchange http) {
		SecurityService securityService = SERVICE_MANAGER.getSecurityService();
		
		AuthorisationServerLogger.debug("Start security controll.");

		UUID userId;
		String validationKey;

		List<String> values = http.getRequestHeaders().get(HEADERS_USERID);
		AuthorisationServerLogger.debug("User-Id: " + values);
		if (values == null || values.size() != 1) {
			return new ResponseMessage(ResponseType.UNAUTHORIZED, new ErrorDTO("Nicht eingeloggt."));
		}
		userId = UUID.fromString(values.get(0));

		values = http.getRequestHeaders().get(HEADERS_VALIDATION_KEY);
		AuthorisationServerLogger.debug("Validation-Key: " + values);
		if (values == null || values.size() != 1) {
			return new ResponseMessage(ResponseType.UNAUTHORIZED, new ErrorDTO("Nicht eingeloggt."));
		}
		validationKey = values.get(0);

		securityService.validateUser(userId, validationKey);
		ValidatedUser currentUser = securityService.currentUser();

		if (securityService == null || currentUser == null) {
			AuthorisationServerLogger.debug("Validation failed");
			return new ResponseMessage(ResponseType.UNAUTHORIZED, new ErrorDTO("Nicht eingeloggt."));
		}
		AuthorisationServerLogger.debug("Validation succeeded");
		return null;
	}

	@Override
	public int order() {
		return -10;
	}

}
