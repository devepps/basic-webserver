package authorisation.web.dto;

import json.JSON;

@JSON(name = "user")
public class UserDTO {

	private String username;
	private String password;
	private String eMail;

	public UserDTO() {
	}

	public UserDTO(String username, String password, String eMail) {
		this.username = username;
		this.password = password;
		this.eMail = eMail;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getEMail() {
		return eMail;
	}

	@Override
	public String toString() {
		return "UserDTO [username=" + username + ", password=" + "****" + ", eMail=" + eMail + "]";
	}

}
