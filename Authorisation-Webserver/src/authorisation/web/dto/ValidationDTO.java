package authorisation.web.dto;

import json.JSON;
import userSystem.data.ValidatedUser;

@JSON(name = "validation")
public class ValidationDTO {

	private String userId;
	private String validationKey;

	public ValidationDTO() {
	}

	public ValidationDTO(String userId, String validationKey) {
		this.userId = userId;
		this.validationKey = validationKey;
	}

	public ValidationDTO(ValidatedUser validatedUser) {
		this.userId = validatedUser.getUser().getId() == null ? null : validatedUser.getUser().getId().toString();
		this.validationKey = validatedUser.getValidationKey();
	}

	public String getUserId() {
		return userId;
	}

	public String getValidationKey() {
		return validationKey;
	}

	@Override
	public String toString() {
		return "ValidatedUserDTO [userId=" + userId + ", validationKey=" + validationKey + "]";
	}
}
