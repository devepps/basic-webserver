package authorisation.web.dto;

import json.JSON;

@JSON(name = "login")
public class LoginDTO {
	
	private String username;
	private String password;

	public LoginDTO() {
	}

	public LoginDTO(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public String toString() {
		return "LoginDTO [username=" + username + ", password=" + "****" + "]";
	}
}
