package authorisation.web.dto;

import json.JSON;

@JSON(name = "validatedUser")
public class ValidatedUserDTO {

	private String userId;
	private String validationKey;
	private String username;

	public ValidatedUserDTO() {
	}

	public ValidatedUserDTO(String userId, String validationKey, String username) {
		super();
		this.userId = userId;
		this.validationKey = validationKey;
		this.username = username;
	}

	public String getUserId() {
		return userId;
	}

	public String getValidationKey() {
		return validationKey;
	}

	public String getUsername() {
		return username;
	}

	@Override
	public String toString() {
		return "ValidatedUserDTO [userId=" + userId + ", validationKey=" + validationKey + ", username=" + username
				+ "]";
	}

}
