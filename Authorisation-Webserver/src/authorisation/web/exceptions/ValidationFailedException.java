package authorisation.web.exceptions;

import authorisation.web.dto.ValidationDTO;
import server.data.exceptions.HTTPException;
import server.data.type.ResponseType;

public class ValidationFailedException extends HTTPException {

	private static final long serialVersionUID = 1L;

	public ValidationFailedException(ValidationDTO validation) {
		super(ResponseType.BAD_REQUEST, "Der Anmeldungscode ist fehlerhaft oder geh�rt nicht zum angefragten Benutzer.");
	}

}
