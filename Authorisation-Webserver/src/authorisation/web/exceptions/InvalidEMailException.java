package authorisation.web.exceptions;

import server.data.exceptions.HTTPException;
import server.data.type.ResponseType;

public class InvalidEMailException extends HTTPException {

	private static final long serialVersionUID = 1L;

	public InvalidEMailException(String email) {
		super(ResponseType.BAD_REQUEST, "Die E-Mail \"" + email + "\" ist fehlerhaft.");
	}

}
