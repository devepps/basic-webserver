package authorisation.web.exceptions;

import server.data.exceptions.HTTPException;
import server.data.type.ResponseType;

public class NoPermissionException extends HTTPException {

	private static final long serialVersionUID = 1L;

	public NoPermissionException() {
		super(ResponseType.FORBIDDEN, "Keine Berechtigung.");
	}
}
