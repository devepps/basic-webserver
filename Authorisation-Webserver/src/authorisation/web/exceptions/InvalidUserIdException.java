package authorisation.web.exceptions;

import server.data.exceptions.HTTPException;
import server.data.type.ResponseType;

public class InvalidUserIdException extends HTTPException {

	private static final long serialVersionUID = 1L;

	public InvalidUserIdException(String userId) {
		super(ResponseType.BAD_REQUEST, "Die Userid \"" + userId + "\" ist fehlerhaft.");
	}

}
