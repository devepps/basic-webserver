package authorisation.web.exceptions;

import authorisation.web.dto.LoginDTO;
import server.data.exceptions.HTTPException;
import server.data.type.ResponseType;

public class LoginFailedException extends HTTPException {

	private static final long serialVersionUID = 1L;

	public LoginFailedException(LoginDTO login) {
		super(ResponseType.BAD_REQUEST, "Falscher Benutzername oder Password.");
	}

}
