package authorisation.web.exceptions;

import server.data.exceptions.HTTPException;
import server.data.type.ResponseType;

public class InvalidValidationKeyException extends HTTPException {

	private static final long serialVersionUID = 1L;

	public InvalidValidationKeyException() {
		super(ResponseType.BAD_REQUEST, "Der Anmeldungscode ist fehlerhaft.");
	}

}
