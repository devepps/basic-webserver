package authorisation.web.exceptions;

import server.data.exceptions.HTTPException;
import server.data.type.ResponseType;

public class InvalidPasswordException extends HTTPException {

	private static final long serialVersionUID = 1L;

	public InvalidPasswordException(String password) {
		super(ResponseType.BAD_REQUEST, "Das Password \"" + getAnonymousPassword(password) + "\" ist fehlerhaft.");
	}

	private static String getAnonymousPassword(String password) {
		String generated = "";
		for (int i = 0; i < password.length(); i++) {
			generated += "*";
		}
		return generated;
	}

}
