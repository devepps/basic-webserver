package authorisation.web.exceptions;

import authorisation.web.dto.UserDTO;
import server.data.exceptions.HTTPException;
import server.data.type.ResponseType;

public class UserCreationFailedException extends HTTPException {

	private static final long serialVersionUID = 1L;

	public UserCreationFailedException(UserDTO user) {
		super(ResponseType.BAD_REQUEST, "E-Mail oder Benutzername schon vergeben.");
	}

}
