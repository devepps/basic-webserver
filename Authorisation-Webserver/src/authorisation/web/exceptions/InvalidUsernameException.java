package authorisation.web.exceptions;

import server.data.exceptions.HTTPException;
import server.data.type.ResponseType;

public class InvalidUsernameException extends HTTPException {

	private static final long serialVersionUID = 1L;

	public InvalidUsernameException(String username) {
		super(ResponseType.BAD_REQUEST, "Der Benutzername \"" + username + "\" ist fehlerhaft.");
	}

}
