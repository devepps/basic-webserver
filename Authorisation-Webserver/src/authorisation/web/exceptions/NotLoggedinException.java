package authorisation.web.exceptions;

import server.data.exceptions.HTTPException;
import server.data.type.ResponseType;

public class NotLoggedinException extends HTTPException {

	private static final long serialVersionUID = 1L;

	public NotLoggedinException() {
		super(ResponseType.UNAUTHORIZED, "Nicht eingeloggt.");
	}
}
