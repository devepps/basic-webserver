package authorisation.web.service;

public class AuthorisationServiceManager {

	public static final AuthorisationServiceManager SERVICE_MANAGER = new AuthorisationServiceManager();

	private LoginService loginService;
	private SecurityService securityService;

	private AuthorisationServiceManager() {
	}

	public AuthorisationServiceManager create() {
		this.loginService = new LoginService();
		this.securityService = new SecurityService();
		return this;
	}

	public LoginService getLoginService() {
		return loginService;
	}

	public SecurityService getSecurityService() {
		return securityService;
	}

}
