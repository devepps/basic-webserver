package authorisation.web.service;

import java.util.UUID;

import authorisation.java.AuthServer;
import authorisation.logger.AuthorisationServerLogger;
import authorisation.web.dto.ValidationDTO;
import collection.sync.SyncHashMap;
import server.data.interceptor.AfterService;
import server.serverManager.ServerManager;
import userSystem.data.ValidatedUser;

public class SecurityService implements AfterService {

	private final AuthServer server = AuthServer.AUTH_SERVER;
	private final SyncHashMap<Thread, ValidatedUser> validatedUsers = new SyncHashMap<>();

	public SecurityService() {
		ServerManager.SERVER_SERVICE.registerAfterService(this);
	}

	public ValidatedUser currentUser() {
		return this.validatedUsers.get(Thread.currentThread());
	}

	public void validateUser(UUID uuid, String validationKey) {
		ValidatedUser user = server.validateUser(new ValidationDTO(uuid.toString(), validationKey));
		AuthorisationServerLogger.debug("Authorisation [" + uuid + " , " + validationKey + "] has succeeded: " + user);
		this.validatedUsers.put(Thread.currentThread(), user);
	}

	@Override
	public void connectionClose() {
		validatedUsers.remove(Thread.currentThread());
	}

}
