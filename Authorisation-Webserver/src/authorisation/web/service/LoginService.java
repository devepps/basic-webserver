package authorisation.web.service;

import authorisation.java.AuthServer;
import authorisation.logger.AuthorisationServerLogger;
import authorisation.web.dto.LoginDTO;
import authorisation.web.dto.UserDTO;
import authorisation.web.dto.ValidatedUserDTO;
import authorisation.web.dto.ValidationDTO;
import authorisation.web.exceptions.InvalidEMailException;
import authorisation.web.exceptions.InvalidPasswordException;
import authorisation.web.exceptions.InvalidUserIdException;
import authorisation.web.exceptions.InvalidUsernameException;
import authorisation.web.exceptions.InvalidValidationKeyException;
import authorisation.web.exceptions.LoginFailedException;
import authorisation.web.exceptions.UserCreationFailedException;
import authorisation.web.exceptions.ValidationFailedException;
import userSystem.data.ValidatedUser;

public class LoginService {

	private static final int MIN_NAME_LENGTH = 3;
	private static final int MIN_PASSWORD_LENGTH = 6;

	private final AuthServer server = AuthServer.AUTH_SERVER;

	public ValidationDTO loginUser(LoginDTO login) {
		if (login.getUsername() == null || login.getUsername().length() < MIN_NAME_LENGTH) {
			throw new InvalidUsernameException(login.getUsername());
		}
		if (login.getPassword() == null || login.getPassword().length() < MIN_PASSWORD_LENGTH) {
			throw new InvalidPasswordException(login.getPassword());
		}
		ValidatedUser user = server.login(login);
		if (user == null) {
			throw new LoginFailedException(login);
		}
		AuthorisationServerLogger.debug("Login request \"" + login + "\" has succeded.");
		return new ValidationDTO(user);
	}
	
	public ValidationDTO createUser(UserDTO user) {
		if (user.getUsername() == null || user.getUsername().length() < MIN_NAME_LENGTH) {
			throw new InvalidUsernameException(user.getUsername());
		}
		if (user.getPassword() == null || user.getPassword().length() < MIN_PASSWORD_LENGTH) {
			throw new InvalidPasswordException(user.getPassword());
		}
		if (user.getEMail() == null || !user.getEMail().contains("@")
				|| user.getEMail().lastIndexOf('@') > user.getEMail().lastIndexOf('.')) {
			throw new InvalidEMailException(user.getEMail());
		}
		boolean userCreated = server.createUser(user);
		if (!userCreated) {
			throw new UserCreationFailedException(user);
		}
		AuthorisationServerLogger.debug("Register request \"" + user + "\" has succeded.");
		LoginDTO login = new LoginDTO(user.getUsername(), user.getPassword());
		ValidatedUser validatedUser = server.login(login);
		if (validatedUser == null) {
			throw new LoginFailedException(login);
		}
		AuthorisationServerLogger.debug("Login request \"" + login + "\" has succeded.");
		return new ValidationDTO(validatedUser);
	}

	public ValidatedUserDTO validateUser(ValidationDTO validation) {
		if (validation.getValidationKey() == null) {
			throw new InvalidValidationKeyException();
		}
		if (validation.getUserId() == null || validation.getUserId() == null) {
			throw new InvalidUserIdException(validation.getUserId());
		}
		ValidatedUser user = server.validateUser(validation);
		if (user == null) {
			throw new ValidationFailedException(validation);
		}
		AuthorisationServerLogger.debug("Validation request \"" + validation + "\" has succeded.");
		return new ValidatedUserDTO(user.getUser().getId().toString(), user.getValidationKey(), user.getUser().getName());
	}

}
